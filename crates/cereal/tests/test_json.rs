use cereal::prelude::*;

#[test]
fn to_json() {
    let send_message = SendMessage {
        chat_id: ChatId(354),
        text: "Testing testing 123".to_string(),
        reply_markup: Some(InlineKeyboard {
            inline_keyboard: vec![vec![
                InlineKeyboardButton {
                    text: "A".to_string(),
                    callback_data: "Hello".to_string(),
                },
                InlineKeyboardButton {
                    text: "A".to_string(),
                    callback_data: "Hello".to_string(),
                },
            ]],
        }),
    };
    println!("{}", send_message.to_json());

    let edit_message = EditMessageText {
        chat_id: ChatId(3531),
        message_id: MessageId(541231),
        text: "Testing 23".to_string(),
        parse_mode: Some(ParseMode::Html),
    };
    println!("{}", edit_message.to_json());

    #[cfg(feature = "jiff")]
    {
        let schedule_timing = ScheduleTiming::Daily(jiff::civil::Time::new(14, 15, 1, 7).unwrap());
        let schedule_timing2 = ScheduleTiming::Forever;
        println!("{}", schedule_timing.to_ron());
        println!("{}", schedule_timing2.to_ron());
    }
}

#[derive(Serialise)]
#[cereal(transparent)]
struct ChatId(u64);

#[derive(Serialise)]
struct InlineKeyboardButton {
    text: String,
    callback_data: String,
}

#[derive(Serialise)]
struct InlineKeyboard {
    inline_keyboard: Vec<Vec<InlineKeyboardButton>>,
}

#[derive(Serialise)]
struct SendMessage {
    chat_id: ChatId,
    text: String,
    reply_markup: Option<InlineKeyboard>,
}

#[derive(Serialise)]
#[cereal(transparent)]
struct MessageId(u64);

#[derive(Serialise)]
#[cereal(transparent)]
pub enum ParseMode {
    #[cereal(rename = "HTML")]
    Html,
}

#[derive(Serialise)]
struct EditMessageText {
    chat_id: ChatId,
    message_id: MessageId,
    text: String,
    parse_mode: Option<ParseMode>,
}

#[cfg(feature = "jiff")]
#[derive(Serialise)]
pub enum ScheduleTiming {
    Daily(jiff::civil::Time),
    Forever,
}
