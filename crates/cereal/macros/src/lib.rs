use virtue::prelude::*;

#[proc_macro_derive(Serialise, attributes(cereal))]
pub fn derive_serialise(input: TokenStream) -> TokenStream {
    derive_serialise_inner(input).unwrap_or_else(|error| error.into_token_stream())
}

fn derive_serialise_inner(input: TokenStream) -> Result<TokenStream> {
    let parse = Parse::new(input)?;
    let (mut generator, attributes, input_body) = parse.into_generator();

    let attributes = attributes
        .get_attribute::<TypeAttributes>()?
        .unwrap_or(Default::default());
    let target_name = generator.target_name().to_string();

    generator
        .impl_for("::cereal::Serialise<C>")
        .with_impl_generics(["C: ::cereal::Cereal"])
        .generate_fn("partial_serialise")
        .with_self_arg(FnSelfArg::RefSelf)
        .with_arg("state", "::cereal::SerialState<C>")
        .with_return_type(
            "::core::result::Result<::cereal::SerialState<C, true>, ::cereal::SerialState<C>>",
        )
        .body(|body| {
            match input_body {
                Body::Struct(struct_body) => match struct_body.fields {
                    Some(Fields::Tuple(fields)) => {
                        handle_tuple_struct(body, &attributes, fields)?;
                    }
                    Some(Fields::Struct(fields)) => {
                        handle_struct(body, &attributes, &target_name, fields)?;
                    }
                    None => {}
                },
                Body::Enum(enum_body) => {
                    handle_enum(body, &attributes, &target_name, enum_body.variants)?
                }
            }
            Ok(())
        })?;
    generator.finish()
}

fn handle_tuple_struct(
    body: &mut StreamBuilder,
    attributes: &TypeAttributes,
    fields: Vec<UnnamedField>,
) -> Result<()> {
    if !attributes.transparent {
        panic!("Non-transparent tuple structs are not currently supported");
    }
    if fields.len() != 1 {
        panic!("Transparent tuple structs must contain exactly one item");
    }

    if fields.len() == 1 {
        body.ident_str("self")
            .punct('.')
            .lit_usize(0)
            .punct('.')
            .ident_str("partial_serialise")
            .group(Delimiter::Parenthesis, |body| {
                body.ident_str("state");
                Ok(())
            })?;
    }
    Ok(())
}

fn handle_struct(
    body: &mut StreamBuilder,
    attributes: &TypeAttributes,
    target_name: &str,
    fields: Vec<(Ident, UnnamedField)>,
) -> Result<()> {
    if attributes.transparent {
        panic!("Transparent non-tuple structs are not currently supported");
    }

    body.ident_str("Ok").group(Delimiter::Parenthesis, |body| {
        body.ident_str("state")
            .punct('.')
            .ident_str("open_struct")
            .group(Delimiter::Parenthesis, |body| {
                body.lit_str(target_name);
                Ok(())
            })?
            .punct('.');

        for (name, _) in fields {
            body.ident_str("push_field")
                .group(Delimiter::Parenthesis, |body| {
                    body.lit_str(name.to_string())
                        .punct(',')
                        .punct('&')
                        .ident_str("self")
                        .punct('.')
                        .ident(name);
                    Ok(())
                })?
                .punct('.');
        }

        body.ident_str("close_struct")
            .group(Delimiter::Parenthesis, |_| Ok(()))?;
        Ok(())
    })?;
    Ok(())
}

fn handle_enum(
    body: &mut StreamBuilder,
    attributes: &TypeAttributes,
    target_name: &str,
    variants: Vec<EnumVariant>,
) -> Result<()> {
    body.ident_str("Ok").group(Delimiter::Parenthesis, |body| {
        body.ident_str("match")
            .punct('*')
            .ident_str("self")
            .group(Delimiter::Brace, |body| {
                for variant in variants {
                    let var_attributes = variant
                        .attributes
                        .get_attribute::<VariantAttributes>()?
                        .unwrap_or(Default::default());

                    let variant_name = variant.name.to_string();
                    body.ident_str(target_name).puncts("::").ident(variant.name);

                    if let Some(ref fields) = variant.fields {
                        match fields {
                            Fields::Tuple(fields) => {
                                body.group(Delimiter::Parenthesis, |body| {
                                    for i in 0..fields.len() {
                                        body.ident_str(format!("v{i}")).punct(',');
                                    }
                                    Ok(())
                                })?;
                            }
                            _ => panic!("Enum structs are not currently supported"),
                        }
                    }

                    body.puncts("=>")
                        .ident_str("state")
                        .punct('.')
                        .ident_str("open_enum")
                        .group(Delimiter::Parenthesis, |body| {
                            let variant_name = if let Some(name) = var_attributes.rename {
                                name
                            } else {
                                variant_name
                            };

                            let text = if attributes.transparent {
                                variant_name
                            } else {
                                format!("{target_name}::{variant_name}")
                            };

                            body.lit_str(text);
                            Ok(())
                        })?;

                    if let Some(fields) = variant.fields {
                        match fields {
                            Fields::Tuple(fields) => {
                                body.punct('.')
                                    .ident_str("open_tuple")
                                    .group(Delimiter::Parenthesis, |_| Ok(()))?;
                                for i in 0..fields.len() {
                                    body.punct('.').ident_str("push_value").group(
                                        Delimiter::Parenthesis,
                                        |body| {
                                            body.punct('&').ident_str(format!("v{i}"));
                                            Ok(())
                                        },
                                    )?;
                                }
                            }
                            _ => panic!("Enum structs are not currently supported"),
                        }
                    }

                    body.punct('.')
                        .ident_str("close_enum")
                        .group(Delimiter::Parenthesis, |_| Ok(()))?
                        .punct(',');
                }
                Ok(())
            })?;
        Ok(())
    })?;
    Ok(())
}

#[derive(Default)]
struct TypeAttributes {
    transparent: bool,
}

impl FromAttribute for TypeAttributes {
    fn parse(group: &Group) -> Result<Option<Self>> {
        use virtue::utils::{ParsedAttribute, parse_tagged_attribute};

        let parsed = parse_tagged_attribute(group, "cereal")?;
        Ok(if let Some(parsed) = parsed {
            let mut attributes = TypeAttributes::default();
            for attr in parsed {
                match attr {
                    ParsedAttribute::Tag(ident) if ident.to_string() == "transparent" => {
                        attributes.transparent = true;
                    }
                    _ => {}
                }
            }
            Some(attributes)
        } else {
            None
        })
    }
}

#[derive(Default)]
struct VariantAttributes {
    rename: Option<String>,
}

impl FromAttribute for VariantAttributes {
    fn parse(group: &Group) -> Result<Option<Self>> {
        use virtue::utils::{ParsedAttribute, parse_tagged_attribute};

        let parsed = parse_tagged_attribute(group, "cereal")?;
        Ok(if let Some(parsed) = parsed {
            let mut attributes = VariantAttributes::default();
            for attr in parsed {
                match attr {
                    ParsedAttribute::Property(ident, literal) if ident.to_string() == "rename" => {
                        attributes.rename = Some(
                            literal
                                .to_string()
                                .strip_prefix('"')
                                .expect("#[cereal(rename = \"...\")] expects a string")
                                .strip_suffix('"')
                                .expect("#[cereal(rename = \"...\")] expects a string")
                                .to_string(),
                        );
                    }
                    _ => {}
                }
            }
            Some(attributes)
        } else {
            None
        })
    }
}
