use crate::{Cereal, SerialState, Serialise};

pub trait Number: ToString {}

macro_rules! impl_number {
    ($($ty:ty),*) => {
        $(
            impl Number for $ty {}

            impl<C: Cereal> Serialise<C> for $ty {
                fn partial_serialise(
                    &self,
                    state: SerialState<C>,
                ) -> Result<SerialState<C, true>, SerialState<C>> {
                    Ok(state.push_number(*self))
                }
            }
        )*
    };
}

impl_number!(u8, u16, u32, u64, u128, usize);
impl_number!(i8, i16, i32, i64, i128, isize);
impl_number!(f32, f64);
