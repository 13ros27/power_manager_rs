use crate::{Cereal, SerialState, Serialise};
use jiff::civil::Time;

impl<C: Cereal> Serialise<C> for Time {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        Ok(state.push_string(&self.to_string()))
    }
}
