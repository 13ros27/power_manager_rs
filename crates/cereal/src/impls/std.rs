use crate::{Cereal, SerialState, Serialise};
use std::{
    collections::{HashMap, HashSet},
    time::Duration,
};

impl<C: Cereal> Serialise<C> for String {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        Ok(state.push_string(self))
    }
}

impl<C: Cereal, T: Serialise<C>> Serialise<C> for Vec<T> {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        Ok(state.open_list().push_values(self).close_list())
    }
}

impl<C: Cereal, T: Serialise<C>> Serialise<C> for HashSet<T> {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        Ok(state.open_list().push_values(self).close_list())
    }
}

impl<C: Cereal, T: Serialise<C>> Serialise<C> for Option<T> {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        if let Some(inner) = self {
            inner.partial_serialise(state)
        } else {
            Err(state)
        }
    }
}

// TODO: Should be HashMap<K, V, S>
impl<C: Cereal, K: Serialise<C>, V: Serialise<C>> Serialise<C> for HashMap<K, V> {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        Ok(state.open_map().push_pairs(self).close_map())
    }
}

impl<C: Cereal> Serialise<C> for Duration {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        let mut text = self.as_nanos().to_string();
        text.push_str("ns");
        Ok(state.push_string(&text))
    }
}

impl<C: Cereal, A: Serialise<C>, B: Serialise<C>> Serialise<C> for (A, B) {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        Ok(state
            .open_tuple()
            .push_value(&self.0)
            .push_value(&self.1)
            .close_tuple())
    }
}

impl<C: Cereal, T: Serialise<C>> Serialise<C> for &T {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        (*self).partial_serialise(state)
    }
}
