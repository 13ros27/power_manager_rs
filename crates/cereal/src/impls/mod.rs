#[cfg(feature = "jiff")]
mod jiff;
mod std;

use crate::{Cereal, SerialState, Serialise};

pub struct Dynamic<F>(pub F);

impl<C, F> Serialise<C> for Dynamic<F>
where
    C: Cereal,
    F: Fn(SerialState<C>) -> SerialState<C, true>,
{
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        Ok(self.0(state))
    }
}
