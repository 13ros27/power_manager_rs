mod impls;
mod number;
pub mod serialise;

#[cfg(feature = "derive")]
pub use cereal_macros::Serialise;
pub use impls::Dynamic;
pub use serialise::{SerialState, Serialise, ToConciseJson, ToConciseRon, ToJson, ToRon};

pub mod prelude {
    #[cfg(feature = "derive")]
    pub use super::Serialise;

    pub use super::{ToConciseJson, ToConciseRon, ToJson, ToRon};
}

pub trait Cereal: Sized {
    const PRETTY: bool;

    const STRUCT_INCLUDE_NAME: bool;
    const STRUCT_OPEN: char;
    const STRUCT_WRAP_KEY: Option<char>;
    const STRUCT_CLOSE: char;

    const TUPLE_OPEN: char;
    const TUPLE_CLOSE: char;

    const ENUM_WRAP: Option<char>;
}

pub struct Json<const PRETTY: bool>;

impl<const PRETTY: bool> Cereal for Json<PRETTY> {
    const PRETTY: bool = PRETTY;

    const STRUCT_INCLUDE_NAME: bool = false;
    const STRUCT_OPEN: char = '{';
    const STRUCT_WRAP_KEY: Option<char> = Some('"');
    const STRUCT_CLOSE: char = '}';

    const TUPLE_OPEN: char = '[';
    const TUPLE_CLOSE: char = ']';

    const ENUM_WRAP: Option<char> = Some('"');
}

pub struct Ron<const PRETTY: bool>;

impl<const PRETTY: bool> Cereal for Ron<PRETTY> {
    const PRETTY: bool = PRETTY;

    const STRUCT_INCLUDE_NAME: bool = PRETTY;
    const STRUCT_OPEN: char = '(';
    const STRUCT_WRAP_KEY: Option<char> = None;
    const STRUCT_CLOSE: char = ')';

    const TUPLE_OPEN: char = '(';
    const TUPLE_CLOSE: char = ')';

    const ENUM_WRAP: Option<char> = None;
}
