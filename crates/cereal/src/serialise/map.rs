use super::{SerialState, SerialStateWrapper, Serialise};
use crate::Cereal;

pub struct SerialMap<C: Cereal>(SerialStateWrapper<C>);

impl<C: Cereal> SerialMap<C> {
    pub(super) fn new(state: SerialState<C>) -> Self {
        Self(SerialStateWrapper::new(state, true))
    }

    pub fn add_pair<K: Serialise<C>, V: Serialise<C>>(&mut self, key: &K, value: &V) {
        self.0.reborrow(|mut state, first| {
            let initial_len = state.text.len();
            if !*first {
                state.push_inner(',');
                state.push_newline();
            }

            let mut state = match key.partial_serialise(state) {
                Ok(state) => state,
                Err(mut state) => {
                    // SAFETY: All we do with the vector is shrink it to a byte count that we know was valid UTF-8
                    let vec = unsafe { state.text.as_mut_vec() };
                    vec.truncate(initial_len);
                    return state;
                }
            };

            state.push_inner_str(":");
            if C::PRETTY {
                state.push_inner(' ');
            }

            match value.partial_serialise(state.empty()) {
                Ok(state) => {
                    *first = false;
                    state.empty()
                }
                Err(mut state) => {
                    // SAFETY: All we do with the vector is shrink it to a byte count that we know was valid UTF-8
                    let vec = unsafe { state.text.as_mut_vec() };
                    vec.truncate(initial_len);
                    state
                }
            }
        });
    }

    pub fn push_pair<K: Serialise<C>, V: Serialise<C>>(mut self, key: &K, value: &V) -> Self {
        self.add_pair(key, value);
        self
    }

    pub fn push_pairs<'k, 'v, K: Serialise<C> + 'k, V: Serialise<C> + 'v>(
        mut self,
        pairs: impl IntoIterator<Item = (&'k K, &'v V)>,
    ) -> Self {
        for (key, value) in pairs {
            self = self.push_pair(key, value);
        }
        self
    }

    pub fn close_map(self) -> SerialState<C, true> {
        let mut state = self.0.state();
        state.dedent();
        state.push_newline();
        state.push_inner('}');
        state.fill()
    }
}
