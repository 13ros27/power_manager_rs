use super::{SerialState, SerialStateWrapper, Serialise};
use crate::Cereal;

pub struct SerialStruct<C: Cereal>(SerialStateWrapper<C>);

impl<C: Cereal> SerialStruct<C> {
    pub(super) fn new(state: SerialState<C>) -> Self {
        Self(SerialStateWrapper::new(state, true))
    }

    pub fn add_field<T: Serialise<C>>(&mut self, field_name: &str, field: &T) {
        self.0.reborrow(|mut state, first| {
            let initial_len = state.text.len();
            if !*first {
                state.push_inner(',');
                state.push_newline();
            }
            if let Some(c) = C::STRUCT_WRAP_KEY {
                state.push_inner(c)
            }
            state.push_inner_str(field_name);
            if let Some(c) = C::STRUCT_WRAP_KEY {
                state.push_inner(c)
            }
            state.push_inner(':');
            if C::PRETTY {
                state.push_inner(' ');
            }
            match field.partial_serialise(state) {
                Ok(state) => {
                    *first = false;
                    state.empty()
                }
                Err(mut state) => {
                    // SAFETY: All we do with the vector is shrink it to a byte count that we know was valid UTF-8
                    let vec = unsafe { state.text.as_mut_vec() };
                    vec.truncate(initial_len);
                    state
                }
            }
        });
    }

    pub fn push_field<T: Serialise<C>>(mut self, field_name: &str, field: &T) -> Self {
        self.add_field(field_name, field);
        self
    }

    pub fn close_struct(self) -> SerialState<C, true> {
        let mut state = self.0.state();
        state.dedent();
        state.push_newline();
        state.push_inner(C::STRUCT_CLOSE);
        state.fill()
    }
}
