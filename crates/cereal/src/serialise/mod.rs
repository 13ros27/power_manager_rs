mod enm;
mod list;
mod map;
mod strct;
mod tuple;

use crate::{Cereal, Json, Ron, number::Number};
use core::{
    marker::PhantomData,
    mem::{self, MaybeUninit},
};
pub use {
    enm::SerialEnum, list::SerialList, map::SerialMap, strct::SerialStruct, tuple::SerialTuple,
};

pub trait Serialise<C: Cereal> {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>>;

    fn serialise(&self) -> String {
        match self.partial_serialise(Default::default()) {
            Ok(filled) => filled.text,
            Err(empty) => empty.text,
        }
    }
}

pub struct SerialState<C: Cereal, const FULL: bool = false> {
    text: String,
    depth: usize,
    newline: bool,
    _marker: PhantomData<C>,
}

impl<C: Cereal> Default for SerialState<C> {
    fn default() -> Self {
        Self {
            text: String::new(),
            depth: 0,
            newline: true,
            _marker: PhantomData,
        }
    }
}

impl<C: Cereal> SerialState<C> {
    pub fn push_string(mut self, text: &str) -> SerialState<C, true> {
        self.push_inner('"');
        self.push_inner_str(text);
        self.push_inner('"');
        self.fill()
    }

    pub fn push_number<T: Number>(mut self, number: T) -> SerialState<C, true> {
        self.push_inner_str(&number.to_string());
        self.fill()
    }

    pub fn push_bool(mut self, value: bool) -> SerialState<C, true> {
        self.push_inner_str(if value { "true" } else { "false" });
        self.fill()
    }

    pub fn open_map(mut self) -> SerialMap<C> {
        self.push_inner('{');
        self.push_newline();
        self.indent();
        SerialMap::new(self)
    }

    pub fn open_struct(mut self, name: &str) -> SerialStruct<C> {
        if C::STRUCT_INCLUDE_NAME {
            self.push_inner_str(name);
        }
        self.push_inner(C::STRUCT_OPEN);
        self.push_newline();
        self.indent();
        SerialStruct::new(self)
    }

    pub fn open_list(mut self) -> SerialList<C> {
        self.push_inner('[');
        SerialList::new(self)
    }

    pub fn open_tuple(mut self) -> SerialTuple<C> {
        self.push_inner(C::TUPLE_OPEN);
        SerialTuple::new(self)
    }

    pub fn open_enum(mut self, name: &str) -> SerialEnum<C> {
        if let Some(c) = C::ENUM_WRAP {
            self.push_inner(c);
        }
        self.push_inner_str(name);
        SerialEnum::new(self)
    }

    fn fill(self) -> SerialState<C, true> {
        SerialState {
            text: self.text,
            depth: self.depth,
            newline: self.newline,
            _marker: Default::default(),
        }
    }
}

impl<C: Cereal> SerialState<C, true> {
    fn empty(self) -> SerialState<C, false> {
        SerialState {
            text: self.text,
            depth: self.depth,
            newline: self.newline,
            _marker: Default::default(),
        }
    }
}

impl<C: Cereal, const FULL: bool> SerialState<C, FULL> {
    fn push_inner(&mut self, character: char) {
        if self.newline {
            self.newline = false;
            self.push_inner_str(&" ".repeat(self.depth));
        }
        self.text.push(character);
    }

    fn push_inner_str(&mut self, text: &str) {
        if self.newline {
            self.newline = false;
            self.push_inner_str(&" ".repeat(self.depth));
        }
        self.text.push_str(text);
    }

    fn push_newline(&mut self) {
        if C::PRETTY {
            self.push_inner('\n');
            self.newline = true;
        }
    }

    fn indent(&mut self) {
        self.depth += 4;
    }

    fn dedent(&mut self) {
        self.depth -= 4;
    }
}

struct SerialStateWrapper<C: Cereal> {
    state: MaybeUninit<SerialState<C>>,
    pub first: bool,
}

impl<C: Cereal> SerialStateWrapper<C> {
    fn new(state: SerialState<C>, first: bool) -> Self {
        Self {
            state: MaybeUninit::new(state),
            first,
        }
    }

    fn state(self) -> SerialState<C> {
        // SAFETY: `self.state` is never left without a value in it
        unsafe { self.state.assume_init() }
    }

    fn reborrow(&mut self, f: impl Fn(SerialState<C>, &mut bool) -> SerialState<C>) {
        let state = mem::replace(&mut self.state, MaybeUninit::uninit());
        // SAFETY: `self.state` is never left without a value in it
        let state = unsafe { state.assume_init() };
        self.state = MaybeUninit::new(f(state, &mut self.first));
    }
}

pub trait ToJson {
    fn to_json(&self) -> String;
}
impl<S: Serialise<Json<true>>> ToJson for S {
    fn to_json(&self) -> String {
        self.serialise()
    }
}
pub trait ToConciseJson {
    fn to_concise_json(&self) -> String;
}
impl<S: Serialise<Json<false>>> ToConciseJson for S {
    fn to_concise_json(&self) -> String {
        self.serialise()
    }
}
pub trait ToRon {
    fn to_ron(&self) -> String;
}
impl<S: Serialise<Ron<true>>> ToRon for S {
    fn to_ron(&self) -> String {
        self.serialise()
    }
}
pub trait ToConciseRon {
    fn to_concise_ron(&self) -> String;
}
impl<S: Serialise<Ron<false>>> ToConciseRon for S {
    fn to_concise_ron(&self) -> String {
        self.serialise()
    }
}
