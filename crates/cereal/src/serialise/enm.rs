use super::{SerialState, SerialStruct, SerialTuple, Serialise};
use crate::Cereal;

pub struct SerialEnum<C: Cereal> {
    state: SerialState<C>,
}

impl<C: Cereal> SerialEnum<C> {
    pub(super) fn new(state: SerialState<C>) -> Self {
        Self { state }
    }

    pub fn open_struct(mut self) -> SerialEnumStruct<C> {
        self.state.push_inner(' ');
        self.state.push_inner(C::STRUCT_OPEN);
        self.state.push_newline();
        self.state.indent();
        SerialEnumStruct {
            strct: SerialStruct::new(self.state),
        }
    }

    pub fn open_tuple(mut self) -> SerialEnumTuple<C> {
        self.state.push_inner('(');
        SerialEnumTuple {
            tuple: SerialTuple::new(self.state),
        }
    }

    pub fn close_enum(mut self) -> SerialState<C, true> {
        if let Some(c) = C::ENUM_WRAP {
            self.state.push_inner(c);
        }
        self.state.fill()
    }
}

pub struct SerialEnumStruct<C: Cereal> {
    strct: SerialStruct<C>,
}

impl<C: Cereal> SerialEnumStruct<C> {
    pub fn add_field<T: Serialise<C>>(&mut self, field_name: &str, field: &T) {
        self.strct.add_field(field_name, field);
    }

    pub fn push_field<T: Serialise<C>>(self, field_name: &str, field: &T) -> Self {
        Self {
            strct: self.strct.push_field(field_name, field),
        }
    }

    pub fn close_enum(self) -> SerialState<C, true> {
        let mut state = self.strct.close_struct();
        if let Some(c) = C::ENUM_WRAP {
            state.push_inner(c);
        }
        state
    }
}

pub struct SerialEnumTuple<C: Cereal> {
    tuple: SerialTuple<C>,
}

impl<C: Cereal> SerialEnumTuple<C> {
    pub fn add_value<T: Serialise<C>>(&mut self, value: &T) {
        self.tuple.add_value(value);
    }

    pub fn push_value<T: Serialise<C>>(self, value: &T) -> Self {
        Self {
            tuple: self.tuple.push_value(value),
        }
    }

    pub fn close_enum(self) -> SerialState<C, true> {
        let mut state = self.tuple.0.state();
        state.push_inner(')');
        if let Some(c) = C::ENUM_WRAP {
            state.push_inner(c);
        }
        state.fill()
    }
}
