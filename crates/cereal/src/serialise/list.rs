use super::{SerialState, SerialStateWrapper, Serialise};
use crate::Cereal;

pub struct SerialList<C: Cereal>(SerialStateWrapper<C>);

impl<C: Cereal> SerialList<C> {
    pub(super) fn new(state: SerialState<C>) -> Self {
        Self(SerialStateWrapper::new(state, true))
    }

    pub fn add_value<T: Serialise<C>>(&mut self, value: &T) {
        self.0.reborrow(|mut state, first| {
            let initial_len = state.text.len();
            if !*first {
                state.push_inner(',');
                state.push_newline();
            }
            match value.partial_serialise(state) {
                Ok(state) => {
                    *first = false;
                    state.empty()
                }
                Err(mut state) => {
                    // SAFETY: All we do with the vector is shrink it to a byte count that we know was valid UTF-8
                    let vec = unsafe { state.text.as_mut_vec() };
                    vec.truncate(initial_len);
                    state
                }
            }
        });
    }

    pub fn push_value<T: Serialise<C>>(mut self, value: &T) -> Self {
        self.add_value(value);
        self
    }

    pub fn push_values<'a, T: Serialise<C> + 'a>(
        mut self,
        values: impl IntoIterator<Item = &'a T>,
    ) -> Self {
        for value in values {
            self = self.push_value(value);
        }
        self
    }

    pub fn close_list(self) -> SerialState<C, true> {
        let mut state = self.0.state();
        state.push_inner(']');
        state.fill()
    }
}
