#![forbid(missing_docs, unsafe_code)]
//! A crate for monitoring and controlling Wallbox Quasar bi-directional car chargers.

/// Modbus addresses for use when manually controlling the charger through
/// [`read_register`](Quasar::read_register) and [`write_register`](Quasar::write_register).
pub mod addresses;
mod interval;
mod mock;
mod quasar;

pub use modbus::Error;

#[cfg(feature = "mock")]
pub use mock::MockCharger as Quasar;
#[cfg(not(feature = "mock"))]
pub use quasar::Quasar;

use std::{fmt::Display, net::IpAddr, time::Duration};

/// A wrapper trait for all the methods on a quasar (except manual register access).
pub trait Charger: Send + Sync {
    /// Create a new charger instance.
    fn new(host: IpAddr) -> Result<Self, Error>
    where
        Self: Sized;

    /// Enables the Pi to control the Quasar, needed before any other write commands.
    ///
    /// This will always stop it from charging/discharging.
    fn take_control(&mut self) -> Result<(), Error>;
    /// Prevents the Pi from controlling the Quasar, passing control back to the user.
    ///
    /// This will always stop it from charging/discharging and end any "disconnect"s.
    fn release_control(&mut self) -> Result<(), Error>;
    /// Temporarily disconnects the Pi from the charger,
    ///  stopping it from charging during this time and releasing the car.
    fn disconnect(&mut self, duration: Duration) -> Result<(), Error>;

    /// Stop the car from charging, ignoring whether this thinks it is or not.
    fn force_stop_charging(&mut self) -> Result<(), Error>;
    /// Stop the car from charging.
    fn stop_charging(&mut self) -> Result<bool, Error>;

    /// Make the car charge/discharge at the given rate unless we are currently "disconnected".
    fn charge_at(&mut self, amps: i8) -> Result<(), Error>;

    /// Change the amount of time between SoC requests, defaults to 2 minutes.
    ///
    /// If set to `None` it will always request when [`read_soc`](Charger::read_soc) is used.
    fn set_soc_timer(&mut self, interval: Option<Duration>);
    /// Read the State of Charge whether the SoC timer has elapsed or not.
    fn force_read_soc(&mut self) -> Result<u8, Error>;
    /// Read the State of Charge, if this was called recently (within SoC timer) it will return the cached value.
    fn read_soc(&mut self) -> Result<u8, Error>;

    /// Read the maximum available current, if it can't it will return the cached value.
    fn read_max_available_current(&mut self) -> u8;

    /// Read the status of the charger.
    fn read_status(&mut self) -> Result<ChargerStatus, Error>;
    /// Read which recoverable error bits are high.
    fn read_recoverable_errors(&mut self) -> Result<impl Iterator<Item = u8>, Error>;
    /// Read which unrecoverable error bits are high.
    fn read_unrecoverable_errors(&mut self) -> Result<impl Iterator<Item = u8>, Error>;
    /// Read the serial number of the charger.
    fn read_serial_number(&mut self) -> Result<u32, Error>;
}

/// The status currently being reported by the charger.
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ChargerStatus {
    /// Ready to charge when it receives a request.
    Ready = 0,
    /// Currently charging.
    Charging = 1,
    /// It has been told to charge/discharge and is waiting for the car to let it.
    WaitingForCarDemand = 2,
    /// There are schedules set in the Wallbox app and it is waiting for the
    /// next one of those before charging/discharging.
    WaitingForNextSchedule = 3,
    /// The user has paused charging/discharging.
    PausedByUser = 4,
    /// The schedule set in the Wallbox app has finished so it has stopped
    /// charging/discharging.
    EndOfSchedule = 5,
    /// The car isn't connected to the charger.
    Disconnected = 6,
    /// An error has occured (see [`read_recoverable_errors`](Charger::read_recoverable_errors)
    /// and [`read_unrecoverable_errors`](Charger::read_unrecoverable_errors) for more details)
    Error = 7,
    /// ???
    InqueuePowerSharing = 8,
    /// ???
    UnconfiguredPowerSharing = 9,
    /// ???
    InqueuePowerBoost = 10,
    /// Currently discharging.
    Discharging = 11,
}

impl Display for ChargerStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ChargerStatus::Ready => "Ready",
                ChargerStatus::Charging => "Charging",
                ChargerStatus::WaitingForCarDemand => "Waiting for car demand",
                ChargerStatus::WaitingForNextSchedule => "Waiting for next schedule",
                ChargerStatus::PausedByUser => "Paused by user",
                ChargerStatus::EndOfSchedule => "End of schedule",
                ChargerStatus::Disconnected => "Disconnected",
                ChargerStatus::Error => "Error",
                ChargerStatus::InqueuePowerSharing => "Inqueue Power Sharing",
                ChargerStatus::UnconfiguredPowerSharing => "Unconfigured Power Sharing",
                ChargerStatus::InqueuePowerBoost => "Inqueue Power Boost",
                ChargerStatus::Discharging => "Discharging",
            }
        )
    }
}

impl TryFrom<u16> for ChargerStatus {
    type Error = ();
    fn try_from(value: u16) -> Result<Self, ()> {
        Ok(match value {
            0 => ChargerStatus::Ready,
            1 => ChargerStatus::Charging,
            2 => ChargerStatus::WaitingForCarDemand,
            3 => ChargerStatus::WaitingForNextSchedule,
            4 => ChargerStatus::PausedByUser,
            5 => ChargerStatus::EndOfSchedule,
            6 => ChargerStatus::Disconnected,
            7 => ChargerStatus::Error,
            8 => ChargerStatus::InqueuePowerSharing,
            9 => ChargerStatus::UnconfiguredPowerSharing,
            10 => ChargerStatus::InqueuePowerBoost,
            11 => ChargerStatus::Discharging,
            _ => Err(())?,
        })
    }
}
