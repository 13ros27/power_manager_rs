use std::{
    sync::Mutex,
    time::{Duration, Instant},
};

pub struct Interval(Option<IntervalInternal>);

struct IntervalInternal {
    duration: Duration,
    finish_time: Mutex<Instant>,
}

impl Interval {
    pub fn new(duration: Option<Duration>) -> Self {
        if let Some(duration) = duration {
            Self(Some(IntervalInternal {
                duration,
                finish_time: Mutex::new(Instant::now() + duration),
            }))
        } else {
            Self(None)
        }
    }

    pub fn has_elapsed(&self) -> bool {
        if let Some(delay) = &self.0 {
            let mut finish_time = delay.finish_time.lock().expect("Lock was poisoned");
            if finish_time.checked_duration_since(Instant::now()).is_none() {
                *finish_time = Instant::now() + delay.duration;
                true
            } else {
                false
            }
        } else {
            true
        }
    }

    pub fn modify_duration(&mut self, new_duration: Option<Duration>) {
        if let Some(new_duration) = new_duration {
            if let Some(delay) = &mut self.0 {
                delay.duration = new_duration;
            } else {
                *self = Self::new(Some(new_duration));
            }
        } else {
            *self = Self(None);
        }
    }
}
