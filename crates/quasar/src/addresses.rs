/// All Modbus addresses that can be written to.
#[derive(Clone, Copy, Debug)]
#[repr(u16)]
pub enum WriteAddress {
    /// Factory set to 502
    CommunicationPort = 0x50,

    /// 0 = Release Control, 1 = Take Control
    Control = 0x51,
    /// 1 = Start Charging, 2 = Stop Charging
    Action = 0x101,

    /// 0 = Don't start when plugged in, 1 = Do (default)
    StartOnConnection = 0x52,
    /// Defaults to 0
    IdleConnectionTimeout = 0x58,

    /// 0 = CurrentSetpoint (default), 1 = PowerSetpoint
    SetpointType = 0x53,
    /// i8 (Amps)
    CurrentSetpoint = 0x102,
    /// i16 (Watts)
    PowerSetpoint = 0x104,

    /// 0 = Don't enable setpoint on idle (default), 1 = Do
    SetpointOnIdle = 0x54,
    /// i8 (Amps) (defaults to 6)
    CurrentSetpointOnIdle = 0x55,
    /// i16 (Watts) (defaults to 0)
    PowerSetpointOnIdle = 0x57,

    /// 0 = Unlocked, 1 = Locked
    ChargerLockState = 0x100,
}

/// All Modbus addresses that can be read from (includes both read-only and read-write addresses).
#[derive(Debug)]
#[repr(u16)]
pub enum ReadAddress {
    // Read Only
    /// Version of the chargers firmware (u16)
    FirmwareVersion = 0x01,
    /// High bits of the serial number of the charger (fixed) (u16)
    SerialNumberHigh = 0x02,
    /// Low bits of the serial number of the charger (fixed) (u16)
    SerialNumberLow = 0x03,

    /// u8 (Amps)
    MaxAvailableCurrent = 0x200,
    /// u16 (Watts)
    MaxAvailablePower = 0x202,

    /// ChargerStatus
    Status = 0x219,
    /// u8[0..100] (%)
    StateOfCharge = 0x21A,

    /// 16 bit-flags
    UnrecoverableHigh = 0x21B,
    /// 16 bit-flags
    UnrecoverableLow = 0x21C,
    /// 16 bit-flags
    RecoverableHigh = 0x21D,
    /// 16 bit-flags
    RecoverableLow = 0x21E,

    /// ???
    CurrentRMSL1 = 0x207,
    /// ???
    VoltageRMSL1 = 0x20A,
    /// ???
    ActivePowerRMSL1 = 0x20E,

    // Read Write
    /// Factory set to 502
    CommunicationPort = 0x50,

    /// 0 = Release Control, 1 = Take Control
    Control = 0x51,
    /// 0 = Stop Charging, 1 = Start Charging
    Action = 0x101,

    /// 0 = Don't start when plugged in, 1 = Do (default)
    StartOnConnection = 0x52,
    /// Defaults to 0
    IdleConnectionTimeout = 0x58,

    /// 0 = CurrentSetpoint (default), 1 = PowerSetpoint
    SetpointType = 0x53,
    /// i8 (Amps)
    CurrentSetpoint = 0x102,
    /// i16 (Watts)
    PowerSetpoint = 0x104,

    /// 0 = Don't enable setpoint on idle (default), 1 = Do
    SetpointOnIdle = 0x54,
    /// i8 (Amps) (defaults to 6)
    CurrentSetpointOnIdle = 0x55,
    /// i16 (Watts) (defaults to 0)
    PowerSetpointOnIdle = 0x57,

    /// 0 = Unlocked, 1 = Locked
    ChargerLockState = 0x100,
}
