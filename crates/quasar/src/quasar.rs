use crate::{
    Charger, ChargerStatus,
    addresses::{ReadAddress, WriteAddress},
    interval::Interval,
};
use modbus::{Client, Config, Error, Result, Transport};
use std::{
    net::IpAddr,
    time::{Duration, Instant},
};

/// The overall charger instance for both monitoring and controlling a Quasar.
pub struct Quasar {
    client: Transport,
    current: i8,            // The current rate of charge
    disconnected: Instant,  // When it is "disconnected" until
    just_reconnected: bool, // Has it only just reconnected (or is currently disconnected)
    soc_interval: Interval,
    soc: Option<u8>,           // Cached State of Charge
    max_available_current: u8, // Cached Max Available Current
}

impl Quasar {
    /// Manually read information about the charger through its Modbus interface.
    pub fn read_register(&mut self, address: ReadAddress) -> Result<u16> {
        self.client
            .read_holding_registers(address as u16, 1)
            .map(|c| c[0])
    }

    /// Manually tell the charger to do something through its Modbus interface.
    ///
    /// This can confuse this crate if misused by leaving the charger in an unexpected state.
    pub fn write_register(&mut self, address: WriteAddress, value: u16) -> Result<()> {
        self.client.write_single_register(address as u16, value)
    }

    fn is_disconnected(&self) -> bool {
        self.disconnected
            .checked_duration_since(Instant::now())
            .is_some()
    }
}

impl Charger for Quasar {
    fn new(host: IpAddr) -> Result<Self> {
        let mut this = Self {
            client: Transport::new_with_cfg(
                &host.to_string(),
                Config {
                    tcp_port: 502,
                    ..Default::default()
                },
            )?,
            current: 0,
            disconnected: Instant::now(),
            just_reconnected: false,
            soc_interval: Interval::new(Some(Duration::from_secs(120))),
            soc: None,
            max_available_current: 32,
        };
        let _ = this.force_read_soc();
        this.read_max_available_current();
        Ok(this)
    }

    fn take_control(&mut self) -> Result<()> {
        self.write_register(WriteAddress::Control, 1)?;
        // Ensure that our charging state is correct and we aren't currently charging
        self.force_stop_charging()?;
        Ok(())
    }

    fn release_control(&mut self) -> Result<()> {
        // Make sure we don't leave it charging
        self.force_stop_charging()?;
        self.write_register(WriteAddress::Control, 0)?;
        // Prevent it from being disconnected because we always want to be able to release control
        self.disconnected = Instant::now();
        Ok(())
    }

    fn disconnect(&mut self, duration: Duration) -> Result<()> {
        // This checks that this duration is longer than the existing one
        if (self.disconnected - duration)
            .checked_duration_since(Instant::now())
            .is_none()
        {
            if !self.is_disconnected() {
                self.charge_at(3)?;
                self.release_control()?;
            }
            self.just_reconnected = true;
            self.disconnected = Instant::now() + duration;
        }
        Ok(())
    }

    fn force_stop_charging(&mut self) -> Result<()> {
        // Make sure the SoC is correct when we stop charging
        self.force_read_soc()?;
        self.write_register(WriteAddress::Action, 2)?;
        self.current = 0;

        // Prevent it from automatically starting when plugged in
        self.write_register(WriteAddress::StartOnConnection, 0)?;
        Ok(())
    }

    fn stop_charging(&mut self) -> Result<bool> {
        Ok(if self.current != 0 {
            self.force_stop_charging()?;
            true
        } else {
            false
        })
    }

    fn charge_at(&mut self, amps: i8) -> Result<()> {
        let amps = if amps.abs() < 3 {
            0
        } else {
            let maximum = self.max_available_current as i8;
            amps.clamp(-maximum, maximum)
        };

        if !self.is_disconnected() && (self.just_reconnected || self.current != amps) {
            self.just_reconnected = false;

            if amps == 0 {
                self.stop_charging()?;
            } else {
                self.write_register(WriteAddress::CurrentSetpoint, amps as u16)?;

                // Start charging
                self.write_register(WriteAddress::Action, 1)?;
                self.write_register(WriteAddress::StartOnConnection, 1)?;
            }
        }
        // Write the new value to current whether or not we are disconnected, but only if the command succeeds
        self.current = amps;
        Ok(())
    }

    fn set_soc_timer(&mut self, interval: Option<Duration>) {
        self.soc_interval.modify_duration(interval);
    }

    fn force_read_soc(&mut self) -> Result<u8> {
        let soc = self.read_register(ReadAddress::StateOfCharge)? as u8;
        self.soc = Some(soc);
        Ok(soc)
    }

    fn read_soc(&mut self) -> Result<u8> {
        if self.soc_interval.has_elapsed() {
            match self.force_read_soc() {
                Ok(soc) => Ok(soc),
                Err(err) => self.soc.ok_or(err),
            }
        } else {
            self.soc.ok_or(Error::InvalidResponse) // This is a bit of a *shrug* error
        }
    }

    fn read_max_available_current(&mut self) -> u8 {
        if let Ok(max_available_current) = self.read_register(ReadAddress::MaxAvailableCurrent) {
            self.max_available_current = max_available_current as u8;
        }
        self.max_available_current
    }

    fn read_status(&mut self) -> Result<ChargerStatus> {
        self.read_register(ReadAddress::Status)?
            .try_into()
            .map_err(|_| Error::InvalidResponse)
    }

    fn read_recoverable_errors(&mut self) -> Result<impl Iterator<Item = u8>> {
        let high_bits = self.read_register(ReadAddress::RecoverableHigh)?;
        let low_bits = self.read_register(ReadAddress::RecoverableLow)?;
        let bits = ((high_bits as u32) << 16) | (low_bits as u32);
        Ok(IterHigh { bits })
    }

    fn read_unrecoverable_errors(&mut self) -> Result<impl Iterator<Item = u8>> {
        let high_bits = self.read_register(ReadAddress::UnrecoverableHigh)?;
        let low_bits = self.read_register(ReadAddress::UnrecoverableLow)?;
        let bits = ((high_bits as u32) << 16) | (low_bits as u32);
        Ok(IterHigh { bits })
    }

    fn read_serial_number(&mut self) -> Result<u32> {
        let high_bits = self.read_register(ReadAddress::SerialNumberHigh)?;
        let low_bits = self.read_register(ReadAddress::SerialNumberLow)?;
        Ok(((high_bits as u32) << 16) | (low_bits as u32))
    }
}

struct IterHigh {
    bits: u32,
}
impl Iterator for IterHigh {
    type Item = u8;
    fn next(&mut self) -> Option<u8> {
        let index = self.bits.trailing_zeros();
        if index != 32 {
            self.bits ^= 1 << index;
            Some(index as u8 + 1)
        } else {
            None
        }
    }
}
