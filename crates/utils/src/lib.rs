#![forbid(missing_docs, unsafe_code)]
//! Provides convenience methods shared between crates, currently just [`round_to`](RoundExt::round_to).

mod round;

pub use round::RoundExt;
