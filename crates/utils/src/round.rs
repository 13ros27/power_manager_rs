/// An ext trait granting a more convenient rounding method to floats.
pub trait RoundExt {
    /// Rounds `Self` to `n` decimal places.
    fn round_to(&self, n: usize) -> Self;
}

impl RoundExt for f32 {
    fn round_to(&self, n: usize) -> Self {
        let multiplier = 10.0_f32.powf(n as f32);
        (self * multiplier).round() / multiplier
    }
}

impl RoundExt for f64 {
    fn round_to(&self, n: usize) -> Self {
        let multiplier = 10.0_f64.powf(n as f64);
        (self * multiplier).round() / multiplier
    }
}
