use crate::{CurrentMonitor, Currents};
use serial_core::{BaudRate::Baud38400, SerialDevice, SerialPortSettings};
use serial_unix::TTYPort;
use std::{io::Read, path::Path, time::Duration};
use utils::RoundExt;

/// The overall current monitor instance for reading current transformer sensors.
pub struct LechacalHat {
    port: TTYPort,
    len: usize,
}

impl CurrentMonitor for LechacalHat {
    fn new(len: usize) -> LechacalHat {
        let mut port = TTYPort::open(Path::new("/dev/ttyAMA0"))
            .expect("Failed to open Lechacal Hat serial port");
        port.set_timeout(Duration::from_secs(10))
            .expect("Failed to set Lechacal Hat timeout");
        let mut settings = port
            .read_settings()
            .expect("Failed to read Lechacal Hat serial settings");
        settings
            .set_baud_rate(Baud38400)
            .expect("Failed to set Lechacal Hat baudrate");
        port.write_settings(&settings)
            .expect("Failed to change Lechacal Hat serial settings");
        Self { port, len }
    }

    fn read(&mut self) -> Option<Currents> {
        let mut buffer = Vec::new();
        if self.port.read_to_end(&mut buffer).is_ok() {
            buffer.reverse();
            let mut start = None;
            let mut end = None;

            for (i, character) in buffer.iter().enumerate() {
                if *character == b'\n' {
                    if end.is_none() {
                        end = Some(buffer.len() - i - 1);
                    } else {
                        start = Some(buffer.len() - i);
                        break;
                    }
                }
            }
            if start.is_none() {
                start = Some(0);
            }

            buffer.reverse();
            let data = buffer[start?..end?]
                .iter()
                .map(|n| *n as char)
                .collect::<String>();
            let split_data = data.split(' ').skip(1).collect::<Vec<_>>();
            let mut currents = Vec::with_capacity(split_data.len());
            for n in split_data {
                currents.push((n.parse::<f32>().ok()? / 240.0).round_to(4));
            }

            if currents.len() != self.len {
                None
            } else {
                Some(Currents::new(currents))
            }
        } else {
            None
        }
    }
}
