#![forbid(missing_docs, unsafe_code)]
//! A crate for reading from a Lechacal CT sensor RPi HAT.

mod lechacal;
mod mock;

#[cfg(not(feature = "mock"))]
pub use lechacal::LechacalHat;
#[cfg(feature = "mock")]
pub use mock::MockCurrentMonitor as LechacalHat;

use std::{
    fmt::{Display, Formatter},
    ops::{Deref, DerefMut},
};

/// A wrapper trait to read from the current sensors.
pub trait CurrentMonitor {
    /// Create a new current monitor instance.
    fn new(len: usize) -> Self;
    /// Reads all the currents from the Lechacal HAT, blocking until it sends new results.
    ///
    /// This will return `None` if the result is corrupted in some way.
    fn read(&mut self) -> Option<Currents>;
}

/// Which direction the current is going in (used to calculate a total in [`Currents::combine`]).
#[derive(Clone, Copy)]
pub enum CurrentType {
    /// This is a current producer so it flows into the grid.
    Source = -1,
    /// This is a current consumer so it draws power from the grid.
    Drain = 1,
    /// This could be flowing either way so it is ignored when calculating the total.
    Unknown = 0,
}

impl Display for CurrentType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                CurrentType::Source => "Source",
                CurrentType::Drain => "Drain",
                CurrentType::Unknown => "Unknown",
            }
        )
    }
}

/// A list of all the currents read by the sensors.
#[derive(Clone, Debug)]
pub struct Currents(Box<[f32]>);

impl Currents {
    fn new(vec: Vec<f32>) -> Self {
        Self(vec.into_boxed_slice())
    }

    /// Return the total number of amps the system is draining.
    pub fn combine(&self, types: &'static [CurrentType]) -> f32 {
        assert_eq!(types.len(), self.0.len());

        self.0
            .iter()
            .zip(types.iter())
            .map(|(c, t)| c * (*t as u8) as f32)
            .sum()
    }
}

impl Deref for Currents {
    type Target = [f32];
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Currents {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}
