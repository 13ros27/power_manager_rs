use crate::{CurrentMonitor, Currents};

/// A fake equivalent of the Lechacal HAT current monitor used for testing.
pub struct MockCurrentMonitor(usize);

impl CurrentMonitor for MockCurrentMonitor {
    fn new(len: usize) -> Self {
        Self(len)
    }

    fn read(&mut self) -> Option<Currents> {
        std::thread::sleep(std::time::Duration::from_secs(5));
        Some(Currents::new(
            (1..(self.0 + 1)).map(|n| n as f32).collect::<Vec<_>>(),
        ))
    }
}
