use crate::{
    Bot, ChatId, ChatMessage, InlineKeyboardButton, MessageId, ParseMode, payload, response,
};
use std::mem::{ManuallyDrop, forget};

/// A partially completed message (created by [`Bot::send_message`] or [`ChatMessage::send_message`]).
///
/// This will be sent when it is dropped or [`PartialMessage::mes`] is called.
pub struct PartialMessage {
    bot: ManuallyDrop<Bot>,
    payload: ManuallyDrop<payload::SendMessage>,
}
impl Drop for PartialMessage {
    fn drop(&mut self) {
        // SAFETY: This is in `drop` so it can't be used again
        let payload = unsafe { ManuallyDrop::take(&mut self.payload) };
        let _ = self.bot.post::<()>("sendMessage", payload);

        // SAFETY: This is in `drop` so it can't be used again
        unsafe { ManuallyDrop::drop(&mut self.bot) };
    }
}
impl PartialMessage {
    pub(crate) fn new(bot: Bot, chat_id: ChatId, text: String) -> Self {
        Self {
            bot: ManuallyDrop::new(bot),
            payload: ManuallyDrop::new(payload::SendMessage {
                chat_id,
                text,
                reply_markup: None,
            }),
        }
    }

    /// Sends this partial message and returns its chat and message id.
    pub fn mes(mut self) -> ChatMessage {
        // SAFETY: `self` is forgotten so this is equivalent to being in `drop`
        let bot = unsafe { ManuallyDrop::take(&mut self.bot) };
        // SAFETY: `self` is forgotten so this is equivalent to being in `drop`
        let payload = unsafe { ManuallyDrop::take(&mut self.payload) };

        let message: response::Message = bot.post("sendMessage", payload).unwrap();
        let chat_message = ChatMessage {
            bot,
            chat_id: message.chat.id,
            msg_id: message.message_id,
        };

        forget(self);

        chat_message
    }

    /// Adds an inline keyboard to this message before it is sent.
    pub fn with_keyboard(mut self, keyboard: Vec<Vec<InlineKeyboardButton>>) -> Self {
        self.payload.reply_markup = Some(payload::InlineKeyboard {
            inline_keyboard: keyboard,
        });
        self
    }
}

/// A partially edited message (created by [`ChatMessage::edit_text`]).
///
/// The message will be edited when this is dropped.
pub struct PartialTextEdit {
    bot: Bot,
    payload: ManuallyDrop<payload::EditMessageText>,
}
impl Drop for PartialTextEdit {
    fn drop(&mut self) {
        // SAFETY: This is in `drop` so it can't be used again
        let payload = unsafe { ManuallyDrop::take(&mut self.payload) };
        let _ = self.bot.post::<()>("editMessageText", payload);
    }
}
impl PartialTextEdit {
    pub(crate) fn new(bot: Bot, chat_id: ChatId, message_id: MessageId, text: String) -> Self {
        Self {
            bot,
            payload: ManuallyDrop::new(payload::EditMessageText {
                chat_id,
                message_id,
                text,
                parse_mode: None,
            }),
        }
    }

    /// Change the parse mode of this message.
    pub fn with_parse_mode(mut self, parse_mode: ParseMode) -> Self {
        self.payload.parse_mode = Some(parse_mode);
        self
    }
}
