use crate::{ChatId, InlineKeyboardButton, MessageId, ParseMode, UpdateId};
use cereal::Serialise;
use std::time::Duration;

#[derive(Serialise)]
pub struct SendMessage {
    pub chat_id: ChatId,
    pub text: String,
    pub reply_markup: Option<InlineKeyboard>,
}

#[derive(Serialise)]
pub struct InlineKeyboard {
    pub inline_keyboard: Vec<Vec<InlineKeyboardButton>>,
}

#[derive(Serialise)]
pub struct EditMessageText {
    pub chat_id: ChatId,
    pub message_id: MessageId,
    pub text: String,
    pub parse_mode: Option<ParseMode>,
}

#[derive(Serialise)]
pub struct EditMessageReplyMarkup {
    pub chat_id: ChatId,
    pub message_id: MessageId,
    pub reply_markup: Option<InlineKeyboard>,
}

#[derive(Default, Serialise)]
pub struct GetUpdates {
    pub offset: Option<UpdateId>,
    pub limit: Option<u8>,
    pub timeout: Option<Duration>,
    pub allowed_updates: Option<Vec<String>>,
}
