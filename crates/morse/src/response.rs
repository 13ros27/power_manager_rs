use crate::{ChatId, MessageId, UpdateId};
use nanoserde::DeJson;

#[derive(DeJson, Debug)]
pub struct General<T: DeJson> {
    pub ok: bool,
    pub result: T,
}
impl<T: DeJson + Default> Default for General<T> {
    fn default() -> Self {
        Self {
            ok: false,
            result: T::default(),
        }
    }
}

#[derive(DeJson, Debug)]
pub struct Update {
    pub update_id: UpdateId,
    pub message: Option<Message>,
    pub callback_query: Option<CallbackQuery>,
}

#[derive(DeJson, Clone, Debug)]
pub struct Message {
    pub message_id: MessageId,
    pub chat: Chat,
    pub text: Option<String>,
    pub entities: Option<Vec<MessageEntity>>,
}

#[derive(DeJson, Clone, Debug)]
pub struct CallbackQuery {
    pub message: MaybeInaccessibleMessage,
    pub data: String,
}

#[derive(DeJson, Clone, Debug)]
pub struct MaybeInaccessibleMessage {
    pub chat: Chat,
    pub message_id: MessageId,
}

#[derive(DeJson, Clone, Debug)]
pub struct Chat {
    pub id: ChatId,
}

#[derive(DeJson, Clone, Debug)]
pub struct MessageEntity {
    #[nserde(rename = "type")]
    pub type_: EntityType,
    pub offset: u64,
    pub length: u64,
}

#[derive(DeJson, Clone, Debug, PartialEq)]
#[nserde(transparent)]
pub enum EntityType {
    #[nserde(rename = "bot_command")]
    BotCommand,
}
