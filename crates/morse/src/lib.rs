#![allow(clippy::question_mark)] // Does not play nice with nanoserde::DeSer
#![forbid(missing_docs)]
//! A fairly barebones Telegram Bot handler.

#[cfg(feature = "multipart")]
mod multipart;
mod partial;
mod payload;
mod response;

use cereal::prelude::*;
use nanoserde::{DeJson, DeRon};
use std::{
    ops::Deref,
    path::Path,
    sync::{
        Arc,
        atomic::{AtomicU64, Ordering},
    },
    thread::{self, JoinHandle},
    time::Duration,
};
use ureq::{Agent, AgentBuilder};

pub use partial::{PartialMessage, PartialTextEdit};

/// A core bot instance for sending and editing messages.
///
/// This is very cheap to clone.
#[derive(Clone, Debug)]
pub struct Bot(Arc<BotInternal>);

#[derive(Debug)]
struct BotInternal {
    token: String,
    poll_timeout: Duration,
    agent: Agent,
    last_update: AtomicU64, // Option<UpdateId> via a sentinel (0)
}

impl Bot {
    /// Creates a new bot instance with a given `token` and `timeout`.
    ///
    /// The `timeout` controls how often the bot messages the Telegram servers
    /// when it isn't receiving messages, it doesn't affect its responsiveness.
    pub fn new(token: &str, timeout: Duration) -> Self {
        let agent = AgentBuilder::new()
            .timeout_read(timeout + Duration::from_secs(1))
            .build();
        let internal = BotInternal {
            token: token.to_owned(),
            poll_timeout: timeout,
            agent,
            last_update: AtomicU64::new(0),
        };
        Self(Arc::new(internal))
    }

    /// Spawns a new thread to permanently listen to messages and dispatch them
    /// to the given `command_handler`.
    pub fn spawn(self, command_handler: impl CommandHandler) -> JoinHandle<()> {
        thread::spawn(move || {
            loop {
                for update in self.new_updates() {
                    self.handle_update(&command_handler, update);
                }
            }
        })
    }

    /// Sends a message containing `text` to the given `chat_id`.
    ///
    /// This message will be sent when `PartialMessage` is dropped or
    /// [`PartialMessage::mes`] is called.
    pub fn send_message(&self, chat_id: ChatId, text: String) -> PartialMessage {
        PartialMessage::new(self.clone(), chat_id, text)
    }

    #[cfg(feature = "multipart")]
    /// Sends a given document to the given `chat_id`.
    ///
    /// Returns `false` if the file can't be found.
    pub fn send_document(&self, chat_id: ChatId, filepath: &Path) -> bool {
        let builder = multipart::MultipartBuilder::new()
            .with_text("chat_id", &chat_id.0.to_string())
            .with_file("document", filepath);
        let Ok(builder) = builder else { return false };
        let (content_type, post_data) = builder.finish();
        let _ = self
            .0
            .agent
            .post(&format!(
                "https://api.telegram.org/bot{}/sendDocument",
                self.0.token
            ))
            .set("Content-Type", &content_type)
            .send_bytes(&post_data);
        true
    }

    fn post<T: DeJson>(&self, method: &str, payload: impl ToConciseJson) -> Option<T> {
        response::General::<T>::deserialize_json(
            &self
                .0
                .agent
                .post(&format!(
                    "https://api.telegram.org/bot{}/{}",
                    self.0.token, method
                ))
                .set("Content-Type", "application/json")
                .send_bytes(payload.to_concise_json().as_bytes())
                .ok()?
                .into_string()
                .unwrap(),
        )
        .ok()
        .and_then(|r| if r.ok { Some(r.result) } else { None })
    }

    fn get<T: DeJson>(&self, method: &str, payload: impl ToConciseJson) -> Option<T> {
        response::General::<T>::deserialize_json(
            &self
                .0
                .agent
                .get(&format!(
                    "https://api.telegram.org/bot{}/{}",
                    self.0.token, method
                ))
                .set("Content-Type", "application/json")
                .send_bytes(payload.to_concise_json().as_bytes())
                .ok()?
                .into_string()
                .unwrap(),
        )
        .ok()
        .and_then(|r| if r.ok { Some(r.result) } else { None })
    }

    fn new_updates(&self) -> Vec<response::Update> {
        let last_update = self.0.last_update.load(Ordering::Relaxed);
        let offset = if last_update == 0 {
            None
        } else {
            Some(UpdateId(last_update + 1))
        };
        let updates: Vec<response::Update> = self
            .get(
                "getUpdates",
                payload::GetUpdates {
                    offset,
                    limit: None,
                    timeout: Some(self.0.poll_timeout),
                    allowed_updates: Some(vec![
                        "message".to_string(),
                        "callback_query".to_string(),
                    ]),
                },
            )
            .unwrap_or_default();
        if let Some(last_update) = updates.last() {
            self.0
                .last_update
                .store(last_update.update_id.0, Ordering::Release);
        }
        updates
    }

    fn handle_update(&self, command_handler: &impl CommandHandler, update: response::Update) {
        if let Some(message) = update.message {
            let text = message.text.unwrap_or_default();
            let entities = message.entities.unwrap_or_default();
            let chat_message = ChatMessage {
                bot: self.clone(),
                chat_id: message.chat.id,
                msg_id: message.message_id,
            };
            match entities.first() {
                Some(response::MessageEntity {
                    type_: response::EntityType::BotCommand,
                    offset,
                    length,
                    ..
                }) if *offset == 0 => {
                    let command = Command {
                        msg: chat_message,
                        name: &text[1..*length as usize],
                        text: text[*length as usize..].trim(),
                    };
                    command_handler.handle_command(command);
                }
                _ => command_handler.handle_message(Message {
                    msg: chat_message,
                    text: &text,
                }),
            }
        } else if let Some(callback) = update.callback_query {
            command_handler.handle_callback(Callback {
                msg: ChatMessage {
                    bot: self.clone(),
                    chat_id: callback.message.chat.id,
                    msg_id: callback.message.message_id,
                },
                data: &callback.data,
            });
        }
    }
}

/// A type representing a command sent to this bot.
#[derive(Debug)]
pub struct Command<'a> {
    /// The underlying message, including which chat the command was sent to.
    pub msg: ChatMessage,
    /// The command name (a single word following the slash), this will never be empty.
    pub name: &'a str,
    /// Any text following the command, will often by empty.
    pub text: &'a str,
}
impl Deref for Command<'_> {
    type Target = ChatMessage;
    fn deref(&self) -> &ChatMessage {
        &self.msg
    }
}

/// A type representing a message sent to this bot (this message doesn't contain a `Command`).
#[derive(Debug)]
pub struct Message<'a> {
    /// The underlying message, including which chat the message was sent to.
    pub msg: ChatMessage,
    /// The text contained in the message.
    pub text: &'a str,
}
impl Deref for Message<'_> {
    type Target = ChatMessage;
    fn deref(&self) -> &ChatMessage {
        &self.msg
    }
}

/// A type representing a callback from an inline keyboard.
#[derive(Debug)]
pub struct Callback<'a> {
    /// The underlying message the keyboard is attached to.
    pub msg: ChatMessage,
    /// The callback data the pressed button contained.
    pub data: &'a str,
}
impl Deref for Callback<'_> {
    type Target = ChatMessage;
    fn deref(&self) -> &ChatMessage {
        &self.msg
    }
}

/// A particular message in a particular chat, used for editing and replying to messages.
#[derive(Clone, Debug)]
pub struct ChatMessage {
    bot: Bot,
    /// The id of the chat this message was sent in.
    pub chat_id: ChatId,
    /// The id of this message.
    pub msg_id: MessageId,
}

impl ChatMessage {
    /// Sends a message containing `text` to the same chat as this message.
    ///
    /// This message will be sent when `PartialMessage` is dropped or
    /// [`PartialMessage::mes`] is called.
    pub fn send_message(&self, text: impl Into<String>) -> PartialMessage {
        self.bot.send_message(self.chat_id, text.into())
    }

    #[cfg(feature = "multipart")]
    /// Sends a given document to the same chat as this message.
    ///
    /// Returns `false` if the file can't be found.
    pub fn send_document(&self, filepath: &Path) -> bool {
        self.bot.send_document(self.chat_id, filepath)
    }

    /// Edit the text of this message (if it was sent by this bot, otherwise this will fail).
    ///
    /// The message will be edited when `PartialTextEdit` is dropped.
    pub fn edit_text(&self, new_text: impl Into<String>) -> PartialTextEdit {
        PartialTextEdit::new(self.bot.clone(), self.chat_id, self.msg_id, new_text.into())
    }

    /// Edit the attached inline keyboard on this message (if it was sent by this bot).
    ///
    /// If there is no keyboard it will attach one and if `new_keyboard` is
    /// `None` it will remove an attached keyboard.
    pub fn edit_keyboard(&self, new_keyboard: Option<Vec<Vec<InlineKeyboardButton>>>) {
        self.bot.post::<()>(
            "editMessageReplyMarkup",
            payload::EditMessageReplyMarkup {
                chat_id: self.chat_id,
                message_id: self.msg_id,
                reply_markup: new_keyboard.map(|k| payload::InlineKeyboard { inline_keyboard: k }),
            },
        );
    }

    /// Remove an attached inline keyboard from this message (if it was sent by this bot).
    pub fn remove_keyboard(&self) {
        self.edit_keyboard(None);
    }
}

/// A user implemented trait describing how messages should be handled (passed to [`Bot::spawn`]).
pub trait CommandHandler: Send + 'static {
    #[allow(unused_variables)]
    /// Handles any commands sent to this bot (any messages starting with `/<...>`).
    fn handle_command(&self, command: Command) {}

    #[allow(unused_variables)]
    /// Handles any messages sent to this bot that aren't also commands.
    fn handle_message(&self, message: Message) {}

    #[allow(unused_variables)]
    /// Handles any inline keyboard callbacks (see [`with_keyboard`](PartialMessage::with_keyboard)).
    fn handle_callback(&self, callback: Callback) {}
}

/// The buttons an inline keyboard is constructed from.
#[derive(Serialise)]
pub struct InlineKeyboardButton {
    /// The text displayed on the button.
    pub text: String,
    /// The data returned in [`Callback::data`] when this button is pressed.
    pub callback_data: String, // Because I don't offer other options callback_data is non-optional
}

/// How should this text be parsed.
#[derive(Serialise)]
#[cereal(transparent)]
pub enum ParseMode {
    #[cereal(rename = "HTML")]
    /// This text should be parsed as HTML (simplified HTML).
    Html,
}

#[derive(DeJson, DeRon, Serialise, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cereal(transparent)]
/// The id of a particular chat.
pub struct ChatId(pub u64);
#[derive(DeJson, Serialise, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cereal(transparent)]
/// The id of a particular message.
pub struct MessageId(pub u64);
#[derive(DeJson, Serialise, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cereal(transparent)]
struct UserId(u64);
#[derive(DeJson, Serialise, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cereal(transparent)]
struct UpdateId(u64);
