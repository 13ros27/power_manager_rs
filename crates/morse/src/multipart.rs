use mime::Mime;
use std::{
    fs::File,
    io::{self, prelude::*},
    path::Path,
};
use tinyrand::{RandRange, StdRand};

const BOUNDARY_LEN: usize = 29;

fn random_alphanumeric(len: usize) -> String {
    let mut rand = StdRand::default();
    (0..len)
        .map(|_| {
            (match rand.next_range(0_u16..62) {
                n @ 0..10 => n as u8 + 48,       // 0-9
                n @ 10..36 => n as u8 - 10 + 65, // A-Z
                n @ 36..62 => n as u8 - 36 + 97, // a-z
                _ => unreachable!(),
            }) as char
        })
        .collect()
}

fn mime_filename(path: &Path) -> (Mime, Option<&str>) {
    let content_type = mime_guess::from_path(path);
    let filename = path.file_name().and_then(|name| name.to_str());
    (content_type.first_or_octet_stream(), filename)
}

#[derive(Debug)]
pub struct MultipartBuilder {
    boundary: String,
    inner: Vec<u8>,
    data_written: bool,
}

impl Default for MultipartBuilder {
    fn default() -> Self {
        Self::new()
    }
}

impl MultipartBuilder {
    pub fn new() -> Self {
        Self {
            boundary: random_alphanumeric(BOUNDARY_LEN),
            inner: Vec::new(),
            data_written: false,
        }
    }

    pub fn with_text(mut self, name: &str, value: &str) -> Self {
        self.write_field_headers(name, None, None);
        self.inner.extend(value.bytes());
        self
    }

    pub fn with_file<P: AsRef<Path>>(self, name: &str, path: P) -> io::Result<Self> {
        let path = path.as_ref();
        let (content_type, filename) = mime_filename(path);
        let mut file = File::open(path)?;
        self.add_stream(&mut file, name, filename, Some(content_type))
    }

    fn add_stream<S: Read>(
        mut self,
        stream: &mut S,
        name: &str,
        filename: Option<&str>,
        content_type: Option<Mime>,
    ) -> io::Result<Self> {
        // This is necessary to make sure it is interpreted as a file on the server end.
        let content_type = Some(content_type.unwrap_or(mime::APPLICATION_OCTET_STREAM));
        self.write_field_headers(name, filename, content_type);
        io::copy(stream, &mut self.inner)?;
        Ok(self)
    }

    fn write_boundary(&mut self) {
        if self.data_written {
            self.inner.extend("\r\n".bytes());
        }
        self.inner
            .extend(format!("----{}\r\n", self.boundary).bytes());
    }

    fn write_field_headers(
        &mut self,
        name: &str,
        filename: Option<&str>,
        content_type: Option<Mime>,
    ) {
        self.write_boundary();
        if !self.data_written {
            self.data_written = true;
        }
        self.inner
            .extend(format!("Content-Disposition: form-data; name=\"{name}\"").bytes());
        if let Some(filename) = filename {
            self.inner
                .extend(format!("; filename=\"{filename}\"").bytes());
        }
        if let Some(content_type) = content_type {
            self.inner
                .extend(format!("\r\nContent-Type: {content_type}").bytes());
        }
        self.inner.extend("\r\n\r\n".bytes());
    }

    /// Returns the content_type and post_data
    pub fn finish(mut self) -> (String, Vec<u8>) {
        if self.data_written {
            self.inner.extend("\r\n".bytes());
        }

        // always write the closing boundary, even for empty bodies
        self.inner
            .extend(format!("----{}--\r\n", self.boundary).bytes());
        (
            format!("multipart/form-data; boundary=--{}", self.boundary),
            self.inner,
        )
    }
}
