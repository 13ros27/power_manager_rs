use crate::{config::Config, shared::Info};
use arc_swap::{ArcSwap, Guard};
use jiff::{SignedDuration, Timestamp, Zoned, civil::Date, tz::TimeZone};
use std::{
    fs::File,
    io::{BufRead, BufReader, Write},
    path::{Path, PathBuf},
    sync::{
        Arc,
        atomic::{AtomicU32, Ordering::Relaxed},
    },
};

const FREQ: SignedDuration = SignedDuration::from_secs(15);

/// Logs passed data to a file about every 15 seconds.
pub struct DataLogger {
    file: ArcSwap<PathBuf>,
    last_update: AtomicU32, // Number of seconds into the day
}

impl DataLogger {
    /// Create a new datalogger instance, creating a file header from `config`.
    ///
    /// If a file already exists for today with the same header it will add to
    /// that one otherwise it will create a new file.
    pub fn new(config: &Config) -> Self {
        Self {
            file: ArcSwap::from_pointee(Self::new_file(config)),
            last_update: AtomicU32::new(0),
        }
    }

    /// The file currently being logged to
    pub fn file(&self) -> Guard<Arc<PathBuf>> {
        self.file.load()
    }

    fn new_file(config: &Config) -> PathBuf {
        let header = "Time,".to_string()
            + &config
                .names
                .iter()
                .zip(config.current_types)
                .map(|(n, t)| format!("{n}({t})"))
                .collect::<Vec<_>>()
                .join(",")
            + ",Recommended,Mode,SoC,Metadata";
        let days = (Timestamp::now().as_second() / 86400).to_string();

        let mut i = 1;
        loop {
            let filename = if i == 1 {
                format!("D{days}.csv")
            } else {
                format!("D{days}_{i}.csv")
            };
            let path = config.path.join(Path::new(&format!("data/{filename}")));

            if path.is_file() {
                let fp = File::open(&path).expect("Should be impossible (we just checked)");
                let file_header = BufReader::new(fp).lines().next().transpose().ok().flatten();
                if file_header.as_ref() == Some(&header) {
                    return path;
                }
            } else {
                let mut fp = File::create(&path).expect("/data doesn't exist?");
                write!(fp, "{}", header).expect("I don't know how this would fail?");
                return path;
            }

            i += 1;
        }
    }

    /// Log this info if it has been at least 15 seconds since it last logged.
    pub fn update_info(&self, config: &Config, soc: u8, info: &Info) {
        let current_time = Zoned::now()
            .duration_since(&Zoned::now().start_of_day().unwrap())
            .as_secs() as u32;
        let last_update = self.last_update.load(Relaxed);
        let this_update = last_update + FREQ.as_secs() as u32;
        if current_time < last_update || this_update < current_time {
            if current_time < last_update {
                self.file.swap(Arc::new(Self::new_file(config)));
            }
            self.log_to_file(config, soc, info);
            self.last_update.store(this_update % 86400, Relaxed);
        }
    }

    fn log_to_file(&self, config: &Config, soc: u8, info: &Info) {
        let mut fp = File::options().append(true).open(&**self.file()).unwrap();
        let timestamp = Zoned::now().strftime("%FT%T").to_string(); // ISO8601 (without ms)
        let currents = info
            .currents
            .iter()
            .map(|c| c.to_string())
            .collect::<Vec<_>>()
            .join(",");
        let mode_name = config.mode.short_name();
        write!(
            fp,
            "\n{timestamp},{currents},{},{mode_name},{soc}",
            info.recommended
        )
        .expect("Writing a new line failed?");
    }

    /// Add metadata to the end of the current line in the log file.
    ///
    /// This is normally things like `The user mode has been changed to Off`.
    pub fn add_metadata(&self, metadata: &str) {
        let mut fp = File::options().append(true).open(&**self.file()).unwrap();
        write!(fp, ",{metadata}").expect("Writing metadata failed?");
    }

    /// Gets the path to all log files created on a particular `date`.
    pub fn get_filepaths(config: &Config, date: Date) -> Vec<PathBuf> {
        let mut files = Vec::new();
        let date_stamp: Timestamp = date.to_zoned(TimeZone::UTC).unwrap().into();
        let daynum = date_stamp
            .since(Timestamp::UNIX_EPOCH)
            .unwrap()
            .get_days()
            .to_string();
        for i in 1.. {
            let filename = if i == 1 {
                format!("D{daynum}.csv")
            } else {
                format!("D{daynum}_{i}.csv")
            };
            let path = config.path.join(Path::new(&format!("data/{filename}")));

            if path.is_file() {
                files.push(path);
            } else {
                break;
            }
        }
        files
    }
}
