use std::fmt::{Display, Formatter};

#[derive(Clone, Copy, Default, PartialEq, Eq, Hash)]
#[repr(u8)]
/// Which mode the power manager is in
pub enum Mode {
    #[default]
    /// It will neither charge nor discharge
    Off = 0,
    /// It will charge if it makes sense
    ChargeOnly = 1,
    /// It will charge or discharge if it makes sense
    ChargeDischarge = 2,
    /// It will always charge at max rate (32A)
    MaxCharge = 3,
}

impl Mode {
    /// A short name for the mode, used for `/live`
    pub fn short_name(&self) -> String {
        match self {
            Self::Off => "O",
            Self::ChargeOnly => "CO",
            Self::ChargeDischarge => "CD",
            Self::MaxCharge => "MC",
        }
        .to_string()
    }
}

impl From<u8> for Mode {
    fn from(val: u8) -> Mode {
        match val {
            0 => Mode::Off,
            1 => Mode::ChargeOnly,
            2 => Mode::ChargeDischarge,
            3 => Mode::MaxCharge,
            _ => panic!("Tried to turn an invalid number into Mode"),
        }
    }
}

impl Display for Mode {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Off => "Off",
                Self::ChargeOnly => "Charge Only",
                Self::ChargeDischarge => "Charge Discharge",
                Self::MaxCharge => "Max Charge",
            }
        )
    }
}
