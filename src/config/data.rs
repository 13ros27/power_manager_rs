use super::{Config, Schedule, Setting};
use crate::mode::Mode;
use bytemuck::Pod;

impl Config {
    /// Get the stored data associated with the given `setting`, overriding if
    /// in a relevant mode and returning a default if the setting isn't set.
    pub fn get<T: DataType>(&self, setting: T) -> T::Data {
        // Check if the data is overriden by the mode
        if let Some(data) = self.get_mode_overrides(self.mode, setting) {
            data
        } else {
            let data: Option<T::Data> = self
                .stored
                .schedule
                .get(&setting.into())
                .map(|d| d.get().get());
            T::alter(data, self)
        }
    }

    /// Store a value into one of the settings, overriding any schedules for that setting.
    pub fn set_const<T: DataType>(&mut self, setting: T, data: T::Data) -> &mut Self {
        self.stored
            .schedule
            .insert(setting.into(), Schedule::new_fixed(Data::new(data)));
        self.save();
        self
    }

    fn get_mode_overrides<T: DataType>(&self, mode: Mode, setting: T) -> Option<T::Data> {
        match (mode, setting.into()) {
            (
                Mode::ChargeOnly | Mode::MaxCharge,
                Setting::DischargeValue | Setting::LowDischargeValue,
            ) => Some(Data::new(self.stored.prices.high_day()).get()),
            (Mode::MaxCharge, Setting::MaxPaidSoc) => Some(Data::new(100).get()),
            _ => None,
        }
    }
}

/// A type of data that can be stored in the settings `HashMap`.
pub trait DataType: Copy + Into<Setting> {
    /// The data type that is actually stored.
    type Data: Pod;
    /// Takes the optional data that was stored and returns a non-optional value,
    /// either by unwrapping or substituting a default.
    fn alter(data: Option<Self::Data>, config: &Config) -> Self::Data;
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) struct Data(Box<[u8]>);

impl Data {
    pub fn new<D: Pod>(data: D) -> Self {
        let data_ref = &[data];
        let data_slice: &[u8] = bytemuck::cast_slice(data_ref);
        Self(data_slice.into())
    }

    pub fn get<D: Pod>(&self) -> D {
        let data: &[D] = bytemuck::cast_slice(&self.0);
        data[0]
    }

    pub fn get_mut<D: Pod>(&mut self) -> &mut D {
        let data: &mut [D] = bytemuck::cast_slice_mut(&mut self.0);
        &mut data[0]
    }
}
