macro_rules! setting {
    ($t:ident: $d:ident, $(#[doc = $doc:tt])?) => {
        macros::setting!{{
            $t,
            $d,
            $($doc)?,
            |data: Option<$d>, _| data.expect(&format!("{} should always be set", stringify!($t)))
        }}
    };
    ($t:ident: $d:ident = ^$default:ident, $(#[doc = $doc:tt])?) => {
        macros::setting!{{
            $t,
            $d,
            $($doc)?,
            |data: Option<$d>, config: &Config| data.unwrap_or_else(|| config.get($default))
        }}
    };
    ($t:ident: $d:ident = $default:expr, $(#[doc = $doc:tt])?) => {
        macros::setting!{{
            $t,
            $d,
            $($doc)?,
            |data: Option<$d>, _| data.unwrap_or($default)
        }}
    };
    ({$t:ident, $d:ty, $($doc:tt)?, $alter:expr}) => {
        #[derive(Clone, Copy)]
        $(#[doc = $doc])?
        pub struct $t;

        #[expect(clippy::from_over_into)]
        impl Into<Setting> for $t {
            fn into(self) -> Setting {
                Setting::$t
            }
        }

        impl DataType for $t {
            type Data = $d;
            fn alter(data: Option<$d>, config: &Config) -> $d {
                ($alter)(data, config)
            }
        }
    };
}
pub(super) use setting;

macro_rules! settings {
    ($($t:ident: $d:ident $(= ^$def1:ident)? $(= $def2:expr)?, $(#[doc = $doc:tt])?)*) => {
        $(macros::setting!{$t: $d $(= ^$def1)? $(= $def2)?, $(#[doc = $doc])?})*

        /// All the configurable settings able to be stored in the schedule.
        #[derive(Debug, DeRon, Serialise, PartialEq, Eq, Hash)]
        #[cereal(transparent)]
        pub enum Setting {
            $(
                $(#[doc = $doc])?
                $t
            ),*
        }

        impl Setting {
            fn get_type_id(&self) -> ::core::any::TypeId {
                match self {
                    $(Setting::$t => ::core::any::TypeId::of::<$d>()),*
                }
            }
        }
    };
}
pub(super) use settings;
