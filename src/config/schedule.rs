use super::{Config, DataType, data::Data};
use cereal::Serialise;
use jiff::{SignedDuration, Zoned, civil::Time};
use log::error;
use nanoserde::DeRon;
use std::{ops::Deref, slice::IterMut};

impl Config {
    /// Filters through the schedule of a particular setting looking for a particular field.
    ///
    /// This shouldn't be used mutably unless the caller follows it by a call to [`save`](Config::save).
    /// Prefer to use [`change_schedule`](Config::change_schedule) if you wish to edit the value.
    pub fn match_schedule<T: DataType>(
        &mut self,
        setting: T,
        matcher: impl Fn(&T::Data) -> bool,
    ) -> Option<ScheduleMut> {
        let schedule = self.stored.schedule.get_mut(&setting.into())?;
        for mut schedule_mut in schedule {
            if matcher(&schedule_mut.data().get()) {
                return Some(schedule_mut);
            }
        }
        None
    }

    /// Filters through the schedule of a particular setting looking for a
    /// particular field and then edits that field. This also saves the change.
    pub fn change_schedule<T: DataType>(
        &mut self,
        setting: T,
        matcher: impl Fn(&T::Data) -> bool,
        apply: impl Fn(ScheduleMut),
    ) -> bool {
        if let Some(schedule_mut) = self.match_schedule(setting, matcher) {
            apply(schedule_mut);
            self.save();
            true
        } else {
            false
        }
    }

    /// Add a [`ScheduleTiming::Daily`] item to a particular setting's schedule.
    #[allow(dead_code)]
    pub fn add_daily_schedule<T: DataType>(&mut self, setting: T, time: Time, data: T::Data) {
        let setting_enum = setting.into();
        if let Some(schedule) = self.stored.schedule.get_mut(&setting_enum) {
            schedule.add_daily(Data::new(data), time);
        } else {
            self.stored
                .schedule
                .insert(setting_enum, Schedule::new_daily(Data::new(data), time));
        }
        self.save()
    }
}

/// At what times is the associated data the applicable one.
#[derive(Debug, PartialEq, Serialise)]
pub enum ScheduleTiming {
    /// This value becomes the applied one at this [`Time`] every day.
    Daily(Time),
    /// This setting always has this value, this must be the only item in a schedule.
    Forever,
}

#[derive(Debug)]
pub(super) struct Schedule(NonEmptyVec<(Data, ScheduleTiming)>);

impl Schedule {
    pub(super) fn from_vec(vec: Vec<(Data, ScheduleTiming)>) -> Self {
        Self(NonEmptyVec::from_vec(vec))
    }

    pub(super) fn new_fixed(data: Data) -> Self {
        Self(NonEmptyVec::new((data, ScheduleTiming::Forever)))
    }
    pub(super) fn new_daily(data: Data, time: Time) -> Self {
        Self(NonEmptyVec::new((data, ScheduleTiming::Daily(time))))
    }

    pub(super) fn add_daily(&mut self, data: Data, time: Time) {
        if self.0.first().1 == ScheduleTiming::Forever {
            *self.0.first() = (data, ScheduleTiming::Daily(time));
        } else {
            self.0.push((data, ScheduleTiming::Daily(time)));
        }
    }

    pub(super) fn get(&self) -> &Data {
        let mut best_duration = None;
        let mut best_item = None;
        for (data, timing) in &*self.0 {
            match timing {
                ScheduleTiming::Forever => return data,
                ScheduleTiming::Daily(time) => {
                    let mut time_since = Zoned::now().time().duration_since(*time);
                    if time_since < SignedDuration::ZERO {
                        time_since += SignedDuration::from_hours(24);
                    }
                    if let Some(duration) = best_duration {
                        if time_since < duration {
                            best_duration = Some(time_since);
                            best_item = Some(data);
                        }
                    } else {
                        best_duration = Some(time_since);
                        best_item = Some(data);
                    }
                }
            }
        }

        best_item.unwrap()
    }
}

impl Deref for Schedule {
    type Target = [(Data, ScheduleTiming)];
    fn deref(&self) -> &[(Data, ScheduleTiming)] {
        &self.0
    }
}

impl<'a> IntoIterator for &'a mut Schedule {
    type Item = ScheduleMut<'a>;
    type IntoIter = ScheduleIter<'a>;
    fn into_iter(self) -> Self::IntoIter {
        ScheduleIter(self.0.iter_mut())
    }
}

pub struct ScheduleMut<'a>(&'a mut (Data, ScheduleTiming));

impl ScheduleMut<'_> {
    pub(super) fn data(&mut self) -> &mut Data {
        &mut self.0.0
    }
    pub fn timing(&self) -> &ScheduleTiming {
        &self.0.1
    }

    pub fn set_time(&mut self, time: Time) {
        if let ScheduleTiming::Daily(ref mut old_time) = self.0.1 {
            *old_time = time;
        } else {
            error!("Tried to change the time of a fixed schedule");
        }
    }
}

pub struct ScheduleIter<'a>(IterMut<'a, (Data, ScheduleTiming)>);
impl<'a> Iterator for ScheduleIter<'a> {
    type Item = ScheduleMut<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(ScheduleMut)
    }
}

use non_empty_vec::NonEmptyVec;
mod non_empty_vec {
    use super::*;
    use smallvec::{SmallVec, smallvec};

    #[derive(Debug)]
    pub struct NonEmptyVec<T>(SmallVec<[T; 1]>);

    impl<T> NonEmptyVec<T> {
        pub fn new(item: T) -> Self {
            Self(smallvec![item])
        }

        pub fn from_vec(vec: Vec<T>) -> Self {
            assert!(!vec.is_empty());
            Self(vec.into())
        }

        pub fn first(&mut self) -> &mut T {
            &mut self.0[0]
        }

        pub fn push(&mut self, item: T) {
            self.0.push(item)
        }

        pub fn iter_mut(&mut self) -> IterMut<T> {
            self.0.iter_mut()
        }
    }

    impl<T> core::ops::Deref for NonEmptyVec<T> {
        type Target = SmallVec<[T; 1]>;
        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }
}

impl DeRon for ScheduleTiming {
    fn de_ron(
        s: &mut nanoserde::DeRonState,
        i: &mut core::str::Chars,
    ) -> core::result::Result<Self, nanoserde::DeRonErr> {
        s.ident(i)?;
        core::result::Result::Ok(match s.identbuf.as_ref() {
            "Daily" => {
                s.paren_open(i)?;
                let r = Self::Daily({
                    let r: String = DeRon::de_ron(s, i)?;
                    s.eat_comma_paren(i)?;
                    <Time as core::str::FromStr>::from_str(&r).map_err(|_| nanoserde::DeRonErr {
                        msg: "Error casting time".to_string(),
                        line: 0,
                        col: 0,
                    })?
                });
                s.paren_close(i)?;
                r
            }
            "Forever" => Self::Forever,
            _ => return core::result::Result::Err(s.err_enum(&s.identbuf)),
        })
    }
}
