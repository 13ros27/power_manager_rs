mod data;
mod macros;
mod price;
mod schedule;

use crate::mode::Mode;
use cereal::{Cereal, SerialState, prelude::*};
use data::Data;
use lechacal::CurrentType;
use morse::ChatId;
use nanoserde::DeRon;
use schedule::Schedule;
use std::{
    any::TypeId,
    collections::{HashMap, HashSet},
    path::Path,
};

pub use data::DataType;
pub use price::EnergyPrices;
pub use schedule::ScheduleTiming;

macros::settings! {
    EnergyPrice: f32, /// The price of energy at this time (in pence)
    ChargeCostLimit: f32, /// The maximum amount we are willing to spend to charge the car
    DischargeValue: f32, /// What we value the power in the battery at
    MinDischargeRate: u8 = 3, /// The minimum current (aside from 0) we will send to the charger
    MaxPaidSoc: u8 = 100, /// The soc at which we switch to only charging if it is free (ChargeCostLimit = 0)
    MinDischargeSoc: u8 = 0, /// The soc at which we stop discharging (DischargeValue = INF)
    LowDischargeValue: f32 = ^DischargeValue, /// A value for the power in the battery used when we are recommending below MinDischargeRate (used in LowExport)
    PumpThreshold: f32 = f32::INFINITY, /// If the heat pump is drawing more power than this then the PumpSubtractor is used
    PumpSubtractor: f32 = 0.0, /// How much to subtract from the heat pump current value if above PumpThreshold
}

/// Contains all the configurable settings available.
pub struct Config {
    /// The path to this program, used for finding datalog files
    pub path: Box<Path>,
    /// The names of all the connected current sensors.
    pub names: [&'static str; 5],
    /// The direction of all the connected current sensors.
    pub current_types: [CurrentType; 5],
    stored: ConfigItems,
    /// The mode the power manager is in (defaults to Off).
    pub mode: Mode,
}

#[derive(Debug)]
struct ConfigItems {
    bot_token: String,
    valid_chats: HashSet<ChatId>,
    schedule: HashMap<Setting, Schedule>,
    prices: EnergyPrices,
}

impl Config {
    /// Create a new group of settings, loading any stored data from the file
    /// attached to the passed folder `path`.
    pub fn new(path: &Path, names: [&'static str; 5], current_types: [CurrentType; 5]) -> Self {
        let stored = Self::load(path);
        Self {
            path: path.into(),
            names,
            current_types,
            stored,
            mode: Mode::Off,
        }
    }

    /// Loads the `config.ron` file and returns it, should only be done on startup
    fn load(path: &Path) -> ConfigItems {
        if let Ok(file_contents) = std::fs::read_to_string(path.join("config.ron")) {
            ConfigItems::deserialize_ron(&file_contents).unwrap()
        } else {
            panic!("Could not find config file");
        }
    }

    /// Saves `stored` into `config.ron`, should be done automatically on any change
    pub fn save(&self) {
        let stored = ConfigItems::to_ron(&self.stored);
        // Ignore a failed save
        let _ = std::fs::write(self.path.join("config.ron"), stored);
    }

    /// Check whether a given `chat_id` is in the allowed list.
    pub fn is_valid_chat(&self, chat_id: ChatId) -> bool {
        self.stored.valid_chats.contains(&chat_id)
    }

    /// Add a `chat_id` to the allowed list.
    pub fn add_valid_chat(&mut self, chat_id: ChatId) {
        self.stored.valid_chats.insert(chat_id);
        self.save();
    }

    /// Get the token for the telegram bot (from the stored config file).
    pub fn get_bot_token(&self) -> &String {
        &self.stored.bot_token
    }
}

impl<C: Cereal> Serialise<C> for ConfigItems {
    fn partial_serialise(
        &self,
        state: SerialState<C>,
    ) -> Result<SerialState<C, true>, SerialState<C>> {
        use cereal::Dynamic;

        Ok(state
            .open_struct("ConfigItems")
            .push_field("bot_token", &self.bot_token)
            .push_field("valid_chats", &self.valid_chats)
            .push_field(
                "schedule",
                &Dynamic(|state: SerialState<C>| {
                    let mut map = state.open_map();
                    for (setting, schedule) in &self.schedule {
                        map.add_pair(
                            setting,
                            &Dynamic(|state: SerialState<C>| {
                                let mut list = state.open_list();
                                for (data, timing) in &**schedule {
                                    let type_id = setting.get_type_id();
                                    if type_id == TypeId::of::<f32>() {
                                        list.add_value(&(data.get::<f32>(), timing));
                                    } else if type_id == TypeId::of::<u8>() {
                                        list.add_value(&(data.get::<u8>(), timing));
                                    } else {
                                        unimplemented!();
                                    }
                                }
                                list.close_list()
                            }),
                        );
                    }
                    map.close_map()
                }),
            )
            .push_field("prices", &self.prices)
            .close_struct())
    }
}

impl DeRon for ConfigItems {
    fn de_ron(
        s: &mut nanoserde::DeRonState,
        i: &mut core::str::Chars,
    ) -> Result<Self, nanoserde::DeRonErr> {
        s.ident(i)?; // "ConfigItems"
        s.paren_open(i)?;

        s.ident(i)?; // "bot_token"
        s.colon(i)?;
        let bot_token = String::de_ron(s, i)?;
        s.eat_comma_paren(i)?;

        s.ident(i)?; // "valid_chats"
        s.colon(i)?;
        let valid_chats = Vec::de_ron(s, i)?.into_iter().map(ChatId).collect();
        s.eat_comma_paren(i)?;

        s.ident(i)?; // "schedule"
        s.colon(i)?;
        s.curly_open(i)?;
        let mut schedule = HashMap::new();
        while s.tok != nanoserde::DeRonTok::CurlyClose {
            let setting = Setting::de_ron(s, i)?;
            s.colon(i)?;
            s.block_open(i)?;
            let mut schedule_item = Vec::new();
            while s.tok != nanoserde::DeRonTok::BlockClose {
                s.paren_open(i)?;
                let type_id = setting.get_type_id();
                let data = if type_id == TypeId::of::<f32>() {
                    Data::new(f32::de_ron(s, i)?)
                } else if type_id == TypeId::of::<u8>() {
                    Data::new(u8::de_ron(s, i)?)
                } else {
                    unimplemented!();
                };
                s.eat_comma_paren(i)?;
                let timing = ScheduleTiming::de_ron(s, i)?;
                s.paren_close(i)?;
                s.eat_comma_paren(i)?;
                schedule_item.push((data, timing));
            }
            s.block_close(i)?;
            s.eat_comma_paren(i)?;
            schedule.insert(setting, Schedule::from_vec(schedule_item));
        }
        s.curly_close(i)?;
        s.eat_comma_paren(i)?;

        s.ident(i)?; // "prices"
        s.colon(i)?;
        let prices = EnergyPrices::de_ron(s, i)?;
        s.eat_comma_paren(i)?;
        s.paren_close(i)?;

        Ok(ConfigItems {
            bot_token,
            valid_chats,
            schedule,
            prices,
        })
    }
}
