use super::{Config, EnergyPrice};
use cereal::Serialise;
use nanoserde::DeRon;
use utils::RoundExt;

impl Config {
    /// Get the stored energy prices (and efficiency).
    pub fn get_prices(&self) -> &EnergyPrices {
        &self.stored.prices
    }

    /// Set the stored day price, also updates the value in settings.
    pub fn set_day_price(&mut self, price: f32) {
        let day_price = self.stored.prices.day;
        if let Some(mut schedule_mut) = self.match_schedule(EnergyPrice, |data| data == &day_price)
        {
            *schedule_mut.data().get_mut() = price;
        }

        self.stored.prices.day = price;
        self.save();
    }

    /// Set the stored night price, also updates the value in settings.
    pub fn set_night_price(&mut self, price: f32) {
        let night_price = self.stored.prices.night;
        if let Some(mut schedule_mut) =
            self.match_schedule(EnergyPrice, |data| data == &night_price)
        {
            *schedule_mut.data().get_mut() = price;
        }

        self.stored.prices.night = price;
        self.save();
    }
}

/// The current price of energy and the efficiency of storing it in the car.
#[derive(Clone, Debug, DeRon, Serialise)]
pub struct EnergyPrices {
    /// The price of energy during the day (in pence).
    pub day: f32,
    /// The price of energy during the night (in pence).
    pub night: f32,
    /// The efficiency of storing energy in the car and pulling it back out (0-1).
    pub efficiency: f32,
}

impl EnergyPrices {
    /// A value slightly below the peak energy price (in pence).
    pub fn low_day(&self) -> f32 {
        (self.day - 0.1).round_to(1)
    }

    /// A value slightly above the peak energy price (in pence).
    pub fn high_day(&self) -> f32 {
        (self.day + 0.1).round_to(1)
    }

    /// A value slightly below the off-peak energy price (in pence).
    pub fn low_night(&self) -> f32 {
        (self.night - 0.1).round_to(1)
    }

    /// A value slightly above the off-peak energy price (in pence).
    pub fn high_night(&self) -> f32 {
        (self.night + 0.1).round_to(1)
    }

    /// The off-peak energy price corrected for car battery efficiency (in pence).
    pub fn discharge_rate(&self) -> f32 {
        (self.night / self.efficiency).round_to(1)
    }

    /// The options offered for changing the `/charge_cost_limit`.
    pub fn charge_cost_limits(&self) -> [(&'static str, Option<f32>); 6] {
        [
            ("Free", Some(0.0)),
            ("Below Off Peak", Some(self.low_night())),
            ("Above Off Peak", Some(self.high_night())),
            ("Below Peak", Some(self.low_day())),
            ("Above Peak", Some(self.high_day())),
            ("Custom", None),
        ]
    }

    /// The options offered for changing the `/discharge_value`.
    pub fn discharge_values(&self) -> [(&'static str, Option<(f32, f32)>); 5] {
        [
            ("Free", Some((0.0, 0.0))),
            (
                "Off Peak",
                Some((self.discharge_rate(), self.discharge_rate())),
            ),
            (
                "Low Export",
                Some((
                    self.discharge_rate(),
                    (self.discharge_rate() + self.low_day()) / 2.0,
                )),
            ),
            ("Below Peak", Some((self.low_day(), self.low_day()))),
            ("Custom", None),
        ]
    }

    /// The options offered for changing the `/max_paid_soc`.
    pub fn max_paid_socs(&self) -> [(&'static str, Option<u8>); 5] {
        [
            ("80%", Some(80)),
            ("85%", Some(85)),
            ("90%", Some(90)),
            ("95%", Some(95)),
            ("Custom", None),
        ]
    }

    /// The options offered for changing the `/min_discharge_soc`.
    pub fn min_discharge_socs(&self) -> [(&'static str, Option<u8>); 5] {
        [
            ("20%", Some(20)),
            ("30%", Some(30)),
            ("40%", Some(40)),
            ("50%", Some(50)),
            ("Custom", None),
        ]
    }
}

impl Default for EnergyPrices {
    fn default() -> Self {
        Self {
            day: 30.6,
            night: 7.5,
            efficiency: 0.8,
        }
    }
}
