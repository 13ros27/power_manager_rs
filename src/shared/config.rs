use super::SharedData;
use crate::config::Config;
use std::{
    mem::ManuallyDrop,
    ops::{Deref, DerefMut},
    sync::{RwLockReadGuard, RwLockWriteGuard},
};

impl SharedData {
    /// Get read access to the config.
    pub fn config(&self) -> RwLockReadGuard<Config> {
        self.0.config.read().expect("Config Guard was poisoned")
    }

    /// Get write access to the config, and update change handlers.
    pub fn config_mut(&self) -> impl DerefMut<Target = Config> + use<'_> {
        let config = self.0.config.write().expect("Config Guard was poisoned");
        ConfigMut {
            config: ManuallyDrop::new(config),
            shared_data: *self,
        }
    }
}

struct ConfigMut<'a> {
    config: ManuallyDrop<RwLockWriteGuard<'a, Config>>,
    shared_data: SharedData,
}

impl Deref for ConfigMut<'_> {
    type Target = Config;
    fn deref(&self) -> &Config {
        &self.config
    }
}
impl DerefMut for ConfigMut<'_> {
    fn deref_mut(&mut self) -> &mut Config {
        &mut self.config
    }
}

impl Drop for ConfigMut<'_> {
    fn drop(&mut self) {
        // SAFETY: This is in the destructor
        unsafe { ManuallyDrop::drop(&mut self.config) }
        // We must have dropped config before this otherwise it would clash with `get_info`
        self.shared_data.update_handlers();
    }
}
