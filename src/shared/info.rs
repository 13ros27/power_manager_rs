use super::SharedData;
use lechacal::Currents;
use quasar::Charger;
use std::sync::MutexGuard;
use utils::RoundExt;

/// The current and charging information.
pub struct Info {
    /// All of the current values read by [`lechacal`].
    pub currents: Currents,
    estimated: f32,
    /// The charge rate recommended by [`recommend`](crate::recommend::recommend).
    pub recommended: i8,
    charge_rate: i8,
}

impl Info {
    /// Collects the current and charging information.
    pub fn new(currents: Currents, estimated: f32, recommended: i8, charge_rate: i8) -> Self {
        Self {
            currents,
            estimated,
            recommended,
            charge_rate,
        }
    }
}

impl SharedData {
    fn last_info(&self) -> MutexGuard<Option<Info>> {
        self.0
            .last_info
            .lock()
            .expect("Last Info Guard was poisoned")
    }

    /// Update the information about charging and currents, and log it if useful.
    pub fn update_info(&self, info: Info) {
        self.logger().update_info(
            &self.config(),
            self.charger().read_soc().unwrap_or(0),
            &info,
        );
        *self.last_info() = Some(info);
        self.update_handlers();
    }

    /// Format the latest information nicely to display in `/live`.
    pub fn formatted_current(&self) -> String {
        if let Some(Info {
            currents,
            estimated,
            recommended,
            charge_rate,
        }) = &*self.last_info()
        {
            let mut message: Vec<_> = self
                .config()
                .names
                .into_iter()
                .zip(self.config().current_types)
                .zip(currents.iter())
                .map(|((name, current_type), current)| {
                    format!("{}A: {name} ({current_type})", current.round_to(1))
                })
                .collect();

            message.push(format!("{}A: Estimated", estimated.round_to(1)));
            message.push(format!("{recommended}A: Recommended"));
            message.push(format!("{charge_rate}A: Charge Rate"));

            let soc = self.charger().read_soc().unwrap_or(0);
            if soc == 0 {
                message.push("?%: State of Charge".to_string());
            } else {
                message.push(format!("{soc}%: State of Charge"));
            }

            message.join("\n")
        } else {
            "N/A".to_string()
        }
    }
}
