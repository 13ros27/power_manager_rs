use crate::{
    config::Config,
    datalogger::DataLogger,
    telebot::handlers::{ErasedHandler, HandlerWrapper},
};
use quasar::Quasar;
use std::sync::{Mutex, MutexGuard, RwLock};

mod config;
mod info;

pub use info::Info;

/// All the data that is needed across threads.
#[derive(Clone, Copy)]
pub struct SharedData(&'static DataInternal);

struct DataInternal {
    charger: Mutex<Quasar>,
    config: RwLock<Config>,
    logger: DataLogger,
    last_info: Mutex<Option<Info>>,
    handlers: Mutex<Vec<ErasedHandler>>,
}

impl SharedData {
    /// Create a wrapper to share the `charger`, `config` and `logger` between threads.
    pub fn new(charger: Quasar, config: Config, logger: DataLogger) -> SharedData {
        let data = Box::new(DataInternal {
            charger: Mutex::new(charger),
            config: RwLock::new(config),
            logger,
            last_info: Mutex::new(None),
            handlers: Mutex::new(Vec::new()),
        });
        SharedData(Box::leak(data))
    }

    /// Get read/write access to the charger.
    pub fn charger(&self) -> MutexGuard<Quasar> {
        self.0.charger.lock().expect("Charger Guard was poisoned")
    }

    pub fn logger(&self) -> &DataLogger {
        &self.0.logger
    }

    fn handlers(&self) -> MutexGuard<Vec<ErasedHandler>> {
        self.0.handlers.lock().expect("Handler Guard was poisoned")
    }

    /// Add a change handler to the list to be updated.
    pub fn add_handler(&self, handler: impl HandlerWrapper + 'static) {
        self.handlers().push(Box::new(handler));
    }

    fn update_handlers(&self) {
        let mut handlers = self.handlers();
        let mut i = 0;
        while i < handlers.len() {
            let handler = &mut handlers[i];
            if handler.update(self) {
                let mut handler = handlers.swap_remove(i);
                handler.remove(self);
            } else {
                i += 1;
            }
        }
    }
}
