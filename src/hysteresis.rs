/// Prevents the charger from switching on and off too fast.
///
/// When it switches to negative, positive or zero it will stay there
/// for at least `max_count` ticks.
pub struct OnOff {
    max_count: u8,
    last: i8,
    negative: u8,
    zeros: u8,
    positive: u8,
}

impl OnOff {
    /// Creates a new instance of OnOff.
    ///
    /// `count` controls how many ticks (~5s) it will stay in a charging type
    /// before potentially switching.
    pub fn new(count: u8) -> Self {
        Self {
            max_count: count,
            last: 0,
            negative: 0,
            zeros: 0,
            positive: 0,
        }
    }

    fn reset(&mut self) {
        self.negative = 0;
        self.zeros = 0;
        self.positive = 0;
    }

    /// Applies On-Off hysteresis, returning `recommended` if the hysteresis is
    /// unnecessary or returning an old recommended value.
    pub fn apply(&mut self, recommended: i8) -> i8 {
        if (self.last == 0 && recommended == 0) || self.last.signum() == recommended.signum() {
            self.reset();
            self.last = recommended;
        } else if self.last == 0 {
            if recommended > 0 {
                self.positive += 1;
                self.negative = 0;
            } else {
                self.negative += 1;
                self.positive = 0;
            }
        } else if self.last > 0 {
            self.last = 3;
            if recommended == 0 {
                self.zeros += 1;
            } else {
                self.negative += 1;
            }
        } else {
            self.last = -3;
            if recommended == 0 {
                self.zeros += 1;
            } else {
                self.positive += 1;
            }
        }

        if self.zeros > self.max_count
            || self.positive > self.max_count
            || self.negative > self.max_count
        {
            self.reset();
            self.last = recommended;
        }

        self.last
    }
}

// It's worth testing whether we still need these with `StartOnConnection`?

// pub struct CarConnect {
//     max_count: usize,
//     since_nonzero: usize,
//     car_on: bool,
// }

// impl CarConnect {
//     pub fn new(max_count: usize) -> Self {
//         Self {
//             max_count,
//             since_nonzero: 0,
//             car_on: false,
//         }
//     }

//     pub fn check(&mut self, quasar: &mut Box<dyn Charger>, charge_rate: i64, car_current: f64) {
//         if charge_rate == 0 {
//             self.since_nonzero += 1;
//         } else {
//             self.since_nonzero = 0;
//         }

//         if self.since_nonzero >= self.max_count {
//             if car_current > 0.8 {
//                 if !self.car_on && quasar.get_status() != ChargerStatus::Ready {
//                     quasar.force_stop_charging();
//                 }
//                 self.car_on = true;
//             } else {
//                 self.car_on = false;
//             }
//         }
//     }
// }

// pub struct Misalignment {
//     max_count: usize,
//     since_zero: usize,
//     car_on: bool,
// }

// impl Misalignment {
//     pub fn new(max_count: usize) -> Self {
//         Self {
//             max_count,
//             since_zero: 0,
//             car_on: false,
//         }
//     }

//     pub fn check(&mut self, quasar: &mut Box<dyn Charger>, charge_rate: i64, car_current: f64) {
//         if charge_rate != 0 {
//             self.since_zero += 1;
//         } else {
//             self.since_zero = 0;
//         }

//         if self.since_zero >= self.max_count {
//             if car_current < 0.8 {
//                 if !self.car_on {
//                     quasar.force_stop_charging();
//                 }
//                 self.car_on = true;
//             } else {
//                 self.car_on = false;
//             }
//         }
//     }
// }
