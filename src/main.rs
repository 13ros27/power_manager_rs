//! Uses a bi-directional car charger to balance the power draw of a house.

use crate::{
    config::*,
    datalogger::DataLogger,
    hysteresis::OnOff,
    mode::Mode,
    recommend::recommend,
    shared::{Info, SharedData},
    telebot::TeleBot,
};
use jiff::Zoned;
use lechacal::{CurrentMonitor, CurrentType, LechacalHat};
use log::{LevelFilter, error, warn};
use morse::Bot;
use quasar::{Charger, Quasar};
use std::{
    net::{IpAddr, Ipv4Addr},
    path::Path,
    time::Duration,
};

mod config;
mod datalogger;
mod hysteresis;
mod mode;
mod recommend;
mod shared;
mod telebot;

const NAMES: [&str; 5] = ["Solar", "House", "Car", "Heat Pump", "Grid"];
const CURRENT_TYPES: [CurrentType; 5] = [
    CurrentType::Source,
    CurrentType::Drain,
    CurrentType::Unknown,
    CurrentType::Drain,
    CurrentType::Unknown,
];
const QUASAR_ADDR: IpAddr = IpAddr::V4(Ipv4Addr::new(192, 168, 1, 74));
#[cfg(feature = "mock")]
const FOLDER: &str = "/home/william/Documents/Programming/Git/power_manager/test_env";
#[cfg(not(feature = "mock"))]
const FOLDER: &str = "/home/pi/power_manager_rs";

fn main() {
    simple_logging::log_to_file(
        format!("{FOLDER}/logs/{}.log", Zoned::now().strftime("%FT%T")),
        LevelFilter::Warn,
    )
    .expect("Setting up the logger failed (ironic)");
    std::panic::set_hook(Box::new(|i| {
        telebot::PANIC_FLAG.store(true, std::sync::atomic::Ordering::Relaxed);
        println!("{i}");
        error!("{i}");
        std::process::exit(1);
    }));

    let config = Config::new(Path::new(FOLDER), NAMES, CURRENT_TYPES);
    let data_logger = DataLogger::new(&config);
    let mut on_off_hysteresis = OnOff::new(4);

    // TODO: I suspect this might fail and want a retry sometimes?
    let quasar = Quasar::new(QUASAR_ADDR).expect("Failed to start Modbus");
    let mut current_monitor = LechacalHat::new(NAMES.len());

    let bot = Bot::new(config.get_bot_token(), Duration::from_secs(100));
    let data = SharedData::new(quasar, config, data_logger);
    let tele_bot = TeleBot(data);

    // Start the telegram bot in a new thread so that it doesn't block the main thread
    std::thread::spawn(move || bot.spawn(tele_bot));

    loop {
        let mut currents = if let Some(c) = current_monitor.read() {
            c
        } else {
            error!("Could not read from current monitor");
            continue;
        };
        println!("{:?}", currents);

        // Subtract from the heat pump if it is running and this is setup
        if currents[3] > data.config().get(PumpThreshold) {
            currents[3] -= data.config().get(PumpSubtractor);
        }

        let estimated = currents.combine(&CURRENT_TYPES);
        let soc = data.charger().read_soc().ok();
        let recommended = recommend(estimated, &data.config(), soc);
        let charge_rate = on_off_hysteresis.apply(recommended);

        if data.config().mode != Mode::Off {
            data.charger()
                .charge_at(charge_rate)
                .unwrap_or_else(|e| warn!("Failed to charge with {e}"));
        }

        let info = Info::new(currents, estimated, recommended, charge_rate);
        data.update_info(info);
    }
}
