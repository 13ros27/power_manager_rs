use super::LISTENING;
use crate::{shared::SharedData, telebot::Command};
use morse::CommandHandler;
use std::sync::atomic::{AtomicBool, Ordering::Relaxed};

/// This should be set to `true` if the main thread panics.
pub static PANIC_FLAG: AtomicBool = AtomicBool::new(false);

/// Holds the data for the telegram bot and handles any incoming commands.
pub struct TeleBot(pub SharedData);

impl TeleBot {
    fn check_panic(chat_message: &morse::ChatMessage) {
        if PANIC_FLAG.load(Relaxed) {
            chat_message.send_message("Crashed");
            std::process::exit(1);
        }
    }
}

impl CommandHandler for TeleBot {
    fn handle_command(&self, command: morse::Command) {
        Self::check_panic(&command);

        LISTENING.with_borrow_mut(|l| l.remove(&command.chat_id));
        if let Ok(command_enum) = Command::try_from(command.name) {
            let is_valid_chat = self.0.config().is_valid_chat(command.chat_id);
            if command_enum == Command::Start || is_valid_chat {
                command_enum.on_command(command, self.0);
            }
        }
    }

    fn handle_message(&self, message: morse::Message) {
        Self::check_panic(&message);

        let listening = LISTENING.with_borrow(|l| l.get(&message.chat_id).copied());
        if let Some(cmd) = listening {
            LISTENING.with_borrow_mut(|l| l.remove(&message.chat_id));
            cmd.on_message(message, self.0);
        }
    }

    fn handle_callback(&self, mut callback: morse::Callback) {
        Self::check_panic(&callback);

        callback.remove_keyboard();
        let [command, callback_data] = callback.data.split(' ').collect::<Vec<_>>()[..] else {
            panic!("Callback data formatted incorrectly");
        };
        let command = Command::try_from(
            command
                .parse::<u8>()
                .expect("Callback command formatted incorrectly"),
        )
        .expect("Callback command out of range");
        callback.data = callback_data;
        command.on_button(callback, self.0);
    }
}
