use crate::{config, mode::Mode, shared::SharedData, telebot::*};
use morse::ParseMode;
use std::time::{Duration, Instant};

impl CommandAction for Live {
    fn on_command(&self, _msg: &str) {
        self.send_handled_message(Instant::now() + Duration::from_secs(5 * 60));
    }
}

impl ChangeAction for Live {
    type Info = (String, Mode);
    fn get_info(&self, data: &SharedData) -> Self::Info {
        (data.formatted_current(), data.config().mode)
    }

    fn update_handler(&mut self, info: Self::Info) {
        self.edit_message_text(format!("<b>LIVE ({})</b>\n{}", info.1.short_name(), info.0))
            .with_parse_mode(ParseMode::Html);
    }

    fn remove_handler(&mut self, info: Self::Info) {
        self.edit_message_text(&info.0);
        self.add_buttons([("Continue", ())]);
    }
}

impl ButtonAction for Live {
    type Data = ();
    fn on_button(&self, _callback: ()) {
        self.handle_message(Instant::now() + Duration::from_secs(5 * 60));
    }
}

impl Settings {
    fn cost_text<T: PartialEq>(&self, val: T, consts: &[(&str, T)]) -> String {
        if let Some((name, _)) = consts.iter().find(|(_, v)| v == &val) {
            format!("({}) ", name)
        } else {
            "".to_string()
        }
    }
}

impl CommandAction for Settings {
    fn on_command(&self, _msg: &str) {
        // Track changes to settings for 5 minutes
        self.send_handled_message(Instant::now() + Duration::from_secs(5 * 60));
    }
}

#[derive(Clone, Copy, Default, PartialEq)]
pub struct SettingsInfo {
    mode: Mode,
    ccl: f32,
    dv: f32,
    ldv: f32,
    mps: u8,
    mds: u8,
}

impl ChangeAction for Settings {
    type Info = SettingsInfo;
    fn get_info(&self, data: &SharedData) -> Self::Info {
        SettingsInfo {
            mode: data.config().mode,
            ccl: data.config().get(config::ChargeCostLimit),
            dv: data.config().get(config::DischargeValue),
            ldv: data.config().get(config::LowDischargeValue),
            mps: data.config().get(config::MaxPaidSoc),
            mds: data.config().get(config::MinDischargeSoc),
        }
    }

    fn update_handler(&mut self, info: Self::Info) {
        let charge_cost_limit = self.cost_text(
            Some(info.ccl),
            &self.data.config().get_prices().charge_cost_limits(),
        ) + &format!("{}p", info.ccl);
        let discharge_value = self.cost_text(
            Some((info.dv, info.ldv)),
            &self.data.config().get_prices().discharge_values(),
        ) + &if info.dv == info.ldv {
            format!("{}p", info.dv)
        } else {
            format!("{}p [{}p]", info.dv, info.ldv)
        };

        self.edit_message_text(format!(
            "/mode {}
/charge_cost_limit {}
/discharge_value {}
/max_paid_soc {}%
/min_discharge_soc {}%",
            info.mode, charge_cost_limit, discharge_value, info.mps, info.mds,
        ));
    }
}
