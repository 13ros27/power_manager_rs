use crate::{
    telebot::*,
    {config, config::Config, config::ScheduleTiming},
};
use jiff::civil::Time;
use log::error;

impl CommandAction for EnergyPrice {
    fn on_command(&self, _msg: &str) {
        let day = self.data.config().get_prices().day;
        let night = self.data.config().get_prices().night;
        let Some((day_hour, day_minute)) = get_price_time(&mut self.data.config_mut(), day) else {
            return;
        };
        let Some((night_hour, night_minute)) = get_price_time(&mut self.data.config_mut(), night)
        else {
            return;
        };
        self.send_message(format!(
            "/day_price = {day}p\n/night_price = {night}p\n/day_time = {:02}:{:02}\n/night_time = {:02}:{:02}",
            day_hour, day_minute, night_hour, night_minute
        ))
        ;
    }
}

impl CommandAction for DayPrice {
    fn on_command(&self, _msg: &str) {
        self.send_message(format!(
            "The day energy price is currently set at {}p, please enter a new value:",
            self.data.config().get_prices().day
        ));
        self.listen_for_message();
    }
}

impl MessageAction for DayPrice {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            self.data.config_mut().set_day_price(value);
            self.notify(format!("Changed the day energy price to {value}p"));
        } else {
            self.send_message("Failed to change day energy price");
        }
    }
}

impl CommandAction for NightPrice {
    fn on_command(&self, _msg: &str) {
        self.send_message(format!(
            "The night energy price is currently set at {}p, please enter a new value:",
            self.data.config().get_prices().night
        ));
        self.listen_for_message();
    }
}

impl MessageAction for NightPrice {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            self.data.config_mut().set_night_price(value);
            self.notify(format!("Changed the night energy price to {value}p"));
        } else {
            self.send_message("Failed to change night energy price");
        }
    }
}

fn get_price_time(config: &mut Config, price: f32) -> Option<(i8, i8)> {
    if let Some(schedule_mut) = config.match_schedule(config::EnergyPrice, |data| data == &price) {
        if let ScheduleTiming::Daily(time) = schedule_mut.timing() {
            Some((time.hour(), time.minute()))
        } else {
            error!("Price timings were set to Forever?");
            None
        }
    } else {
        error!("Could not find time for the price {price}p");
        None
    }
}

impl CommandAction for DayTime {
    fn on_command(&self, _msg: &str) {
        let day_price = self.data.config().get_prices().day;
        let time = get_price_time(&mut self.data.config_mut(), day_price);
        if let Some((hour, minute)) = time {
            self.send_message(format!(
                "Day time is currently set at {hour:02}:{minute:02}, please enter a new value:"
            ));
            self.listen_for_message();
        }
    }
}

impl CommandAction for NightTime {
    fn on_command(&self, _msg: &str) {
        let night_price = self.data.config().get_prices().night;
        let time = get_price_time(&mut self.data.config_mut(), night_price);
        if let Some((hour, minute)) = time {
            self.send_message(format!(
                "Night time is currently set at {hour:02}:{minute:02}, please enter a new value:"
            ));
            self.listen_for_message();
        }
    }
}

fn parse_time(msg: &str) -> Option<Time> {
    if let [hour, minute] = msg.split(':').collect::<Vec<_>>()[..] {
        if let Ok(hour) = hour.parse() {
            if let Ok(minute) = minute.parse() {
                return Time::new(hour, minute, 0, 0).ok();
            }
        }
    }
    None
}

impl MessageAction for DayTime {
    fn on_message(&self, msg: &str) {
        if let Some(time) = parse_time(msg) {
            let day_price = self.data.config().get_prices().day;
            if self.data.config_mut().change_schedule(
                config::EnergyPrice,
                |data| data == &day_price,
                |mut schedule_mut| schedule_mut.set_time(time),
            ) {
                self.notify(format!(
                    "Changed the day time to {:02}:{:02}",
                    time.hour(),
                    time.minute()
                ));
                return;
            }
        }

        self.send_message("Failed to change day energy time");
    }
}

impl MessageAction for NightTime {
    fn on_message(&self, msg: &str) {
        if let Some(time) = parse_time(msg) {
            let night_price = self.data.config().get_prices().night;
            if self.data.config_mut().change_schedule(
                config::EnergyPrice,
                |data| data == &night_price,
                |mut schedule_mut| schedule_mut.set_time(time),
            ) {
                self.notify(format!(
                    "Changed the night time to {:02}:{:02}",
                    time.hour(),
                    time.minute()
                ));
                return;
            }
        }

        self.send_message("Failed to change night energy time");
    }
}
