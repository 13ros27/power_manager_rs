use quasar::Charger;
use utils::RoundExt;

use crate::{config, telebot::*};

impl CommandAction for ChargeCostLimit {
    fn on_command(&self, _msg: &str) {
        let mes = format!(
            "The charge cost limit is {}p",
            self.data.config().get(config::ChargeCostLimit).round_to(1)
        );
        let values = self.data.config().get_prices().charge_cost_limits();
        self.send_button_message(&mes, values);
    }
}

impl ButtonAction for ChargeCostLimit {
    type Data = Option<f32>;
    fn on_button(&self, value: Option<f32>) {
        if let Some(value) = value {
            self.data
                .config_mut()
                .set_const(config::ChargeCostLimit, value);
            let text = format!("The charge cost limit has been changed to {value}p");
            self.edit_message_text(&text);
            self.data.logger().add_metadata(&text);
        } else {
            self.edit_message_text("Please enter a charge cost limit:");
            self.listen_for_message();
        }
    }
}

impl MessageAction for ChargeCostLimit {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            self.data
                .config_mut()
                .set_const(config::ChargeCostLimit, value);
            let text = format!("The charge cost limit has been changed to {value}p");
            self.notify(&text);
        } else {
            self.send_message("Please enter a float:");
            self.listen_for_message();
        }
    }
}

impl CommandAction for DischargeValue {
    fn on_command(&self, _msg: &str) {
        let dv = self.data.config().get(config::DischargeValue);
        let ldv = self.data.config().get(config::LowDischargeValue);
        let dis_text = if dv != ldv {
            format!("{}p [{}p]", dv, ldv)
        } else {
            format!("{}p", dv)
        };

        let mes = format!("The discharge value is {dis_text}");
        let values = self.data.config().get_prices().discharge_values();
        self.send_button_message(&mes, values);
    }
}

impl ButtonAction for DischargeValue {
    type Data = Option<(f32, f32)>;
    fn on_button(&self, values: Option<(f32, f32)>) {
        if let Some((dis_value, ldis_value)) = values {
            let mut config_mut = self.data.config_mut();
            config_mut.set_const(config::DischargeValue, dis_value);
            config_mut.set_const(config::LowDischargeValue, ldis_value);
            let text = format!(
                "The discharge value has been changed to {}",
                if dis_value != ldis_value {
                    format!("{dis_value}p [{ldis_value}p]")
                } else {
                    format!("{dis_value}p")
                }
            );
            self.edit_message_text(&text);
            self.data.logger().add_metadata(&text);
        } else {
            self.edit_message_text("Please enter a discharge value:");
            self.listen_for_message();
        }
    }
}

impl MessageAction for DischargeValue {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            let old_dv = self.data.config().get(config::DischargeValue);
            let old_ldv = self.data.config().get(config::LowDischargeValue);
            let mut config_mut = self.data.config_mut();
            let text = if old_dv == old_ldv {
                config_mut.set_const(config::DischargeValue, value);
                config_mut.set_const(config::LowDischargeValue, value);
                format!("The discharge value has been changed to {value}p")
            } else {
                config_mut.set_const(config::DischargeValue, value);
                format!("The discharge value has been changed to {value}p [{old_ldv}p]")
            };
            self.notify(&text);
        } else {
            self.send_message("Please enter a float:");
            self.listen_for_message();
        }
    }
}

impl CommandAction for MinDischargeRate {
    fn on_command(&self, msg: &str) {
        if let Ok(mdr) = msg.parse() {
            self.data
                .config_mut()
                .set_const(config::MinDischargeRate, mdr);
        } else {
            self.notify("Incorrectly formatted command, please specify a min discharge rate");
        }
    }
}

impl CommandAction for MaxPaidSoc {
    fn on_command(&self, _msg: &str) {
        let soc = self
            .data
            .charger()
            .read_soc()
            .map_or("?".to_string(), |n| n.to_string());
        let mes = format!(
            "The max paid SoC is {}%, the current SoC is {}%",
            self.data.config().get(config::MaxPaidSoc),
            soc
        );
        let values = self.data.config().get_prices().max_paid_socs();
        self.send_button_message(mes, values);
    }
}

impl ButtonAction for MaxPaidSoc {
    type Data = Option<u8>;
    fn on_button(&self, mps: Option<u8>) {
        if let Some(mps) = mps {
            self.data.config_mut().set_const(config::MaxPaidSoc, mps);
            let text = format!(
                "The max paid SoC has been changed to {mps}%, the current SoC is {}%",
                self.data
                    .charger()
                    .read_soc()
                    .map_or("?".to_string(), |n| n.to_string())
            );
            self.edit_message_text(&text);
            self.data.logger().add_metadata(&text);
        } else {
            self.edit_message_text("Please enter a max paid SoC:");
            self.listen_for_message();
        }
    }
}

impl MessageAction for MaxPaidSoc {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            self.data.config_mut().set_const(config::MaxPaidSoc, value);
            let text = format!(
                "The max paid SoC has been changed to {value}%, the current SoC is {}%",
                self.data
                    .charger()
                    .read_soc()
                    .map_or("?".to_string(), |n| n.to_string())
            );
            self.notify(&text);
        } else {
            self.send_message("Please enter an integer:");
            self.listen_for_message();
        }
    }
}

impl CommandAction for MinDischargeSoc {
    fn on_command(&self, _msg: &str) {
        let soc = self
            .data
            .charger()
            .read_soc()
            .map_or("?".to_string(), |n| n.to_string());
        let mes = format!(
            "The min discharge SoC is {}%, the current SoC is {}%",
            self.data.config().get(config::MinDischargeSoc),
            soc
        );
        let values = self.data.config().get_prices().min_discharge_socs();
        self.send_button_message(mes, values);
    }
}

impl ButtonAction for MinDischargeSoc {
    type Data = Option<u8>;
    fn on_button(&self, mds: Option<u8>) {
        if let Some(mds) = mds {
            self.data
                .config_mut()
                .set_const(config::MinDischargeSoc, mds);
            let text = format!(
                "The min discharge SoC has been changed to {mds}%, the current SoC is {}%",
                self.data
                    .charger()
                    .read_soc()
                    .map_or("?".to_string(), |n| n.to_string())
            );
            self.edit_message_text(&text);
            self.data.logger().add_metadata(&text);
        } else {
            self.edit_message_text("Please enter a min discharge SoC:");
            self.listen_for_message();
        }
    }
}

impl MessageAction for MinDischargeSoc {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            self.data
                .config_mut()
                .set_const(config::MinDischargeSoc, value);
            let text = format!(
                "The min discharge SoC has been changed to {value}%, the current SoC is {}%",
                self.data
                    .charger()
                    .read_soc()
                    .map_or("?".to_string(), |n| n.to_string())
            );
            self.notify(&text);
        } else {
            self.send_message("Please enter an integer:");
            self.listen_for_message();
        }
    }
}

impl CommandAction for PumpThreshold {
    fn on_command(&self, _msg: &str) {
        self.send_message(format!(
            "The pump threshold is currently {}, please enter a new value:",
            self.data.config().get(config::PumpThreshold)
        ));
        self.listen_for_message();
    }
}

impl MessageAction for PumpThreshold {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            self.data
                .config_mut()
                .set_const(config::PumpThreshold, value);
            self.notify(format!("Changed the pump threshold to {value}"));
        } else {
            self.send_message("Failed to change pump threshold");
        }
    }
}

impl CommandAction for PumpSubtractor {
    fn on_command(&self, _msg: &str) {
        self.send_message(format!(
            "The pump subtractor is currently {}, please enter a new value:",
            self.data.config().get(config::PumpSubtractor)
        ));
        self.listen_for_message();
    }
}

impl MessageAction for PumpSubtractor {
    fn on_message(&self, msg: &str) {
        if let Ok(value) = msg.parse() {
            self.data
                .config_mut()
                .set_const(config::PumpSubtractor, value);
            self.notify(format!("Changed the pump subtractor to {value}"));
        } else {
            self.send_message("Failed to change pump subtractor");
        }
    }
}
