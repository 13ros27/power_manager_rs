use crate::{mode, telebot::*};

impl CommandAction for Mode {
    fn on_command(&self, _msg: &str) {
        self.send_button_message(
            format!("The user mode is currently {}", self.data.config().mode),
            [
                ("Off", mode::Mode::Off),
                ("Charge Only", mode::Mode::ChargeOnly),
                ("Charge Discharge", mode::Mode::ChargeDischarge),
                ("Max Charge", mode::Mode::MaxCharge),
            ],
        );
    }
}

impl ButtonAction for Mode {
    type Data = mode::Mode;
    fn on_button(&self, callback: mode::Mode) {
        self.data.config_mut().mode = callback;
        let mes = format!("The user mode has been changed to {}", callback);
        self.edit_message_text(&mes);
        self.data.logger().add_metadata(&mes);
    }
}

impl CommandAction for Off {
    fn on_command(&self, _msg: &str) {
        self.data.config_mut().mode = mode::Mode::Off;
        self.notify("Set user mode to Off");
    }
}

impl CommandAction for ChargeOnly {
    fn on_command(&self, _msg: &str) {
        self.data.config_mut().mode = mode::Mode::ChargeOnly;
        self.notify("Set user mode to ChargeOnly");
    }
}

impl CommandAction for ChargeDischarge {
    fn on_command(&self, _msg: &str) {
        self.data.config_mut().mode = mode::Mode::ChargeDischarge;
        self.notify("Set user mode to ChargeDischarge");
    }
}

impl CommandAction for MaxCharge {
    fn on_command(&self, _msg: &str) {
        self.data.config_mut().mode = mode::Mode::MaxCharge;
        self.notify("Set user mode to MaxCharge");
    }
}
