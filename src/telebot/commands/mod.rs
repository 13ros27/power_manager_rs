mod handled;
mod modes;
mod prices;
mod settings;

use crate::{datalogger::DataLogger, telebot::*};
use jiff::{SignedDuration, Timestamp, civil::Date, tz::TimeZone};
use quasar::Charger;
use std::time::Duration;

impl CommandAction for Start {
    fn on_command(&self, msg: &str) {
        if msg == "lego" {
            self.data.config_mut().add_valid_chat(self.chat_id());
            self.send_message("Password correct");
        }
    }
}

impl CommandAction for Status {
    fn on_command(&self, _msg: &str) {
        let formatted = self.data.formatted_current();
        self.send_message(formatted);
    }
}

impl CommandAction for Log {
    fn on_command(&self, metadata: &str) {
        if !metadata.is_empty() {
            self.data.logger().add_metadata(metadata);
            self.send_message(format!("Added '{metadata}' to the log"));
        } else {
            self.listen_for_message();
        }
    }
}

impl MessageAction for Log {
    fn on_message(&self, metadata: &str) {
        self.data.logger().add_metadata(metadata);
        self.send_message(format!("Added '{metadata}' to the log"));
    }
}

impl CommandAction for LatestFile {
    fn on_command(&self, _msg: &str) {
        self.send_document(&self.data.logger().file());
    }
}

impl File {
    fn parse_date(&self, msg: &str) -> Result<Date, String> {
        if msg.is_empty() {
            Err("Incorrectly formatted command, please specify a file or date.".to_string())
        } else if let Some(stripped_msg) = msg.strip_prefix('D') {
            if let Ok(date) = stripped_msg.parse::<i64>() {
                Ok(
                    Timestamp::from_jiff_duration(SignedDuration::from_hours(date * 24))
                        .unwrap()
                        .to_zoned(TimeZone::UTC)
                        .date(),
                )
            } else {
                Err("Incorrectly formatted command, expected number following 'D'".to_string())
            }
        } else {
            let error = format!("Date '{msg}' not recognised");
            let split: Vec<_> = msg.split('/').collect();
            let date: Vec<_> = split.iter().flat_map(|m| m.parse::<i16>()).collect();
            if date.len() == split.len() {
                let this_year = Timestamp::now().to_zoned(TimeZone::UTC).year();
                match &date[..] {
                    [day, month, year] => {
                        if year < &100 {
                            let century = (this_year / 100) * 100;
                            Date::new(century + *year, *month as i8, *day as i8).map_err(|_| error)
                        } else {
                            Date::new(*year, *month as i8, *day as i8).map_err(|_| error)
                        }
                    }
                    [day, month] => {
                        Date::new(this_year, *month as i8, *day as i8).map_err(|_| error)
                    }
                    _ => Err(error),
                }
            } else {
                Err(error)
            }
        }
    }
}

impl CommandAction for File {
    fn on_command(&self, msg: &str) {
        match self.parse_date(msg) {
            Ok(datetime) => {
                let files = DataLogger::get_filepaths(&self.data.config(), datetime);
                for file in &files {
                    self.send_document(file);
                }
                if files.is_empty() {
                    self.send_message(format!("No file for '{msg}' found"));
                }
            }
            Err(err) => {
                self.send_message(err);
            }
        }
    }
}

impl CommandAction for ChargerStatus {
    fn on_command(&self, _msg: &str) {
        self.send_message(
            self.data
                .charger()
                .read_status()
                .map_or("Failed to read status".to_string(), |s| s.to_string()),
        );
    }
}

impl CommandAction for Soc {
    fn on_command(&self, _msg: &str) {
        let soc = self.data.charger().read_soc().unwrap_or(0);
        self.send_message(if soc == 0 {
            "Unknown".to_string()
        } else {
            format!("{soc}%")
        });
    }
}

impl CommandAction for Disconnect {
    fn on_command(&self, msg: &str) {
        let secs = msg.parse().unwrap_or(30);
        if self
            .data
            .charger()
            .disconnect(Duration::from_secs(secs))
            .is_ok()
        {
            self.notify(format!("Disconnected for {secs} seconds"));
        } else {
            self.notify("Failed to disconnect");
        }
    }
}

impl CommandAction for More {
    fn on_command(&self, _msg: &str) {
        self.send_message(
            "/log - Add something to the datalog
/latest_file - Download latest datalog file
/file <filename or date> - Download a given datalog file
/charger_status - Get the current charger status
/soc - Get the state of charge of the car
/energy_price - Get the energy prices and times
/min_discharge_rate - Set the minimum discharge rate (defaults to 3)
/pump_threshold - Change the heat pump subtraction threshold
/pump_subtractor - Change how much the heat pump subtracts when above its threshold
/kill - Shut down the program
/more - Self-referential fun",
        );
    }
}

impl CommandAction for Kill {
    fn on_command(&self, _msg: &str) {
        panic!("Kill command sent!");
    }
}
