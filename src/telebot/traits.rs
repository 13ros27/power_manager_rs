use crate::mode::Mode;

pub trait Parsable {
    fn into_string(self) -> String;
    fn from_string(string: &str) -> Self;
}

impl Parsable for () {
    fn into_string(self) -> String {
        String::new()
    }
    fn from_string(_string: &str) {}
}
impl<P: Parsable> Parsable for Option<P> {
    fn into_string(self) -> String {
        self.map_or("N".to_string(), |n| n.into_string())
    }
    fn from_string(string: &str) -> Self {
        if string == "N" {
            None
        } else {
            Some(P::from_string(string))
        }
    }
}
impl<P: Copy + Parsable> Parsable for (P, P) {
    fn into_string(self) -> String {
        self.0.into_string() + "_" + &self.1.into_string()
    }
    fn from_string(string: &str) -> Self {
        let [a, b] = string
            .split('_')
            .map(|n| P::from_string(n))
            .collect::<Vec<_>>()[..]
        else {
            panic!("Shouldn't be possible due to into_string");
        };
        (a, b)
    }
}
impl Parsable for u8 {
    fn into_string(self) -> String {
        self.to_string()
    }
    fn from_string(string: &str) -> Self {
        string
            .parse()
            .expect("Shouldn't be possible due to into_string")
    }
}
impl Parsable for i32 {
    fn into_string(self) -> String {
        self.to_string()
    }
    fn from_string(string: &str) -> Self {
        string
            .parse()
            .expect("Shouldn't be possible due to into_string")
    }
}
impl Parsable for f32 {
    fn into_string(self) -> String {
        self.to_string()
    }
    fn from_string(string: &str) -> Self {
        string
            .parse()
            .expect("Shouldn't be possible due to into_string")
    }
}
impl Parsable for Mode {
    fn into_string(self) -> String {
        (self as u8).to_string()
    }
    fn from_string(string: &str) -> Self {
        let num: u8 = string
            .parse()
            .expect("Shouldn't be possible due to into_string");
        num.into()
    }
}
