use crate::{shared::SharedData, telebot::handlers::Handler};
use morse::{ChatId, ChatMessage, PartialTextEdit};
use std::{path::Path, time::Instant};

commands! {
    Start, /// Is used with a password to tell the bot of the chats existance
    Status, /// Shows the currents and charging info at the current moment (largely superseded by /live)
    Live: Button + Change, /// Shows the currents, charging info and mode, live updating for 5 minutes
    Log: Message, /// Adds the given message as metadata to the end of the datalog
    LatestFile, /// Replies with the latest datalog file
    File, /// Replies with the specified datalog file, either by name or date
    ChargerStatus, /// Shows the status of the charger (as per the ChargerStatus enum)
    Soc, /// Shows the state of charge of the car
    Mode: Button, /// Allows the user to change the user mode with buttons
    Off, /// Changes the user mode to 'Off'
    ChargeOnly, /// Changes the user mode to 'ChargeOnly'
    ChargeDischarge, /// Changes the user mode to 'ChargeDischarge'
    MaxCharge, /// Changes the user mode to 'MaxCharge'
    ChargeCostLimit: Button + Message, /// Changes the charge cost limit, either to a preset or custom value
    DischargeValue: Button + Message, /// Changes the discharge value, either to a preset or custom value
    MinDischargeRate, /// Changes the minimum discharge rate to whatever is passed with the command
    MaxPaidSoc: Button + Message, /// Changes the max paid soc, either to a preset or custom value
    MinDischargeSoc: Button + Message, /// Changes the min discharge soc, either to a preset or custom value
    PumpThreshold: Message, /// Changes the pump threshold to the value passed or replied with
    PumpSubtractor: Message, /// Changes the pump subtractor to the value passed or replied with
    EnergyPrice, /// Shows the day and night energy prices
    DayPrice: Message, /// Sets the day energy price to the value replied with
    NightPrice: Message, /// Sets the night energy price to the value replied with
    DayTime: Message, /// Sets the time when it switches to day price to the value replied with
    NightTime: Message, /// Sets the time when it switches to night price to the value replied with
    Disconnect, /// Disconnects the pi from the charger (by default for 30 seconds)
    Settings: Change, /// Shows a bunch of different settings, live updating for 5 minutes
    More, /// Explains a variety of available commands
    Kill, /// Shuts down the program
}

#[allow(private_bounds)]
pub trait CommandUtils: command::CommandBase {
    fn chat_id(&self) -> ChatId {
        self.msg().chat_id
    }
    fn edit_message_text(&self, text: impl Into<String>) -> PartialTextEdit {
        self.msg().edit_text(text)
    }
    fn send_message(&self, text: impl Into<String>) -> ChatMessage {
        self.msg().send_message(text).mes()
    }
    fn send_document(&self, file: &Path) {
        self.msg().send_document(file);
    }
    fn notify(&self, text: impl Into<String>) -> ChatMessage {
        let text = text.into();
        self.shared_data().logger().add_metadata(&text);
        self.send_message(&text)
    }
}
impl<B: command::CommandBase> CommandUtils for B {}

pub trait CommandAction: CommandUtils {
    fn on_command(&self, text: &str);
}

pub trait ButtonAction: CommandUtils {
    type Data: Parsable;
    fn on_button(&self, callback: Self::Data);
}

pub trait MessageAction: CommandUtils {
    fn on_message(&self, text: &str);
}

pub trait ChangeAction: CommandUtils {
    type Info: Clone + Default + PartialEq + Send;
    fn get_info(&self, data: &SharedData) -> Self::Info;
    fn update_handler(&mut self, info: Self::Info);
    fn remove_handler(&mut self, _info: Self::Info) {}
}

fn create_keyboard<T: ButtonAction>(
    command: Command,
    button_items: impl IntoIterator<Item = (impl Into<String>, T::Data)>,
) -> Vec<Vec<morse::InlineKeyboardButton>> {
    let mut buttons: Vec<Vec<morse::InlineKeyboardButton>> = Vec::new();
    for (text, callback) in button_items.into_iter() {
        let button = morse::InlineKeyboardButton {
            text: text.into(),
            callback_data: format!("{} {}", command as u8, callback.into_string()),
        };
        match buttons.last_mut() {
            Some(row) if row.len() == 1 => row.push(button),
            _ => buttons.push(vec![button]),
        }
    }
    buttons
}
pub trait ButtonUtils: ButtonAction {
    fn send_button_message(
        &self,
        text: impl Into<String>,
        button_items: impl IntoIterator<Item = (impl Into<String>, Self::Data)>,
    ) {
        self.msg()
            .send_message(text)
            .with_keyboard(create_keyboard::<Self>(self.to_command(), button_items));
    }

    fn add_buttons(&self, button_items: impl IntoIterator<Item = (impl Into<String>, Self::Data)>) {
        self.msg().edit_keyboard(Some(create_keyboard::<Self>(
            self.to_command(),
            button_items,
        )));
    }
}
impl<A: ButtonAction> ButtonUtils for A {}

pub trait MessageUtils: MessageAction {
    fn listen_for_message(&self) {
        super::LISTENING.with_borrow_mut(|l| l.insert(self.chat_id(), self.to_command()));
    }
}
impl<A: MessageAction> MessageUtils for A {}

pub trait ChangeUtils: ChangeAction + Clone + Send + 'static {
    fn send_handled_message(&self, until: Instant) {
        let msg = self.send_message("...");
        let mut new_action = Self::new(msg, *self.shared_data());
        let info = new_action.get_info(self.shared_data());
        new_action.update_handler(info);
        new_action.handle_message(until);
    }

    fn handle_message(&self, until: Instant) {
        let handler = Handler::new(self.clone(), until);
        self.shared_data().add_handler(handler);
    }
}
impl<A: ChangeAction + Clone + Send + 'static> ChangeUtils for A {}
