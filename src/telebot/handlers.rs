use crate::{shared::SharedData, telebot::actions::ChangeAction};
use std::time::Instant;

/// A handler that is wrapped around a `ChangeAction` and an expiry data.
pub struct Handler<T: ChangeAction> {
    until: Instant,
    change_action: T,
    last_info: T::Info,
}

impl<T: ChangeAction> Handler<T> {
    /// Creates a new handler wrapped around a change action, lasting until the `until` `Instant`.
    pub fn new(change_action: T, until: Instant) -> Self {
        Self {
            until,
            change_action,
            last_info: T::Info::default(),
        }
    }
}

/// A non-generic change handler that can therefore be stored with different
/// types of `ChangeAction`.
pub trait HandlerWrapper: Send {
    /// Updates the contained `ChangeAction` and returns `true` if it has expired.
    fn update(&mut self, data: &SharedData) -> bool;
    /// Handles any cleanup upon the removal of this `ChangeAction`.
    fn remove(&mut self, data: &SharedData);
}

impl<T: ChangeAction + Send> HandlerWrapper for Handler<T> {
    fn update(&mut self, data: &SharedData) -> bool {
        let info = self.change_action.get_info(data);
        if self.until.checked_duration_since(Instant::now()).is_none() {
            self.change_action.remove_handler(info);
            true
        } else {
            if info != self.last_info {
                self.last_info = info.clone();
                self.change_action.update_handler(info);
            }
            false
        }
    }

    fn remove(&mut self, data: &SharedData) {
        let info = self.change_action.get_info(data);
        self.change_action.remove_handler(info);
    }
}

/// A boxed change handler to make it usable in a `Vec`.
pub type ErasedHandler = Box<dyn HandlerWrapper>;
