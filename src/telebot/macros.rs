macro_rules! mark_muncher {
    ({$($buttons:tt)*} {$($messages:tt)*} $t:tt {Button $($marks:tt)*} $($tail:tt)*) => {
        mark_muncher! {
            {
                $($buttons)*
                (Self::$t, callback, data) => $t::new(callback.msg, data).on_button(<$t as $crate::telebot::ButtonAction>::Data::from_string(callback.data)),
            }
            {$($messages)*}
            $t {$($marks)*}
            $($tail)*
        }
    };
    ({$($buttons:tt)*} {$($messages:tt)*} $t:tt {Message $($marks:tt)*} $($tail:tt)*) => {
        mark_muncher! {
            {$($buttons)*}
            {
                $($messages)*
                (Self::$t, message, data) => $t::new(message.msg, data).on_message(message.text),
            }
            $t {$($marks)*}
            $($tail)*
        }
    };
    ({$($buttons:tt)*} {$($messages:tt)*} $t:tt {Change $($marks:tt)*} $($tail:tt)*) => {
        mark_muncher! {
            {$($buttons)*}
            {$($messages)*}
            $t {$($marks)*}
            $($tail)*
        }
    };
    ({$($buttons:tt)*} {$($messages:tt)*} $t:tt {}, $($tail:tt)*) => {
        mark_muncher! {
            {$($buttons)*}
            {$($messages)*}
            $($tail)*
        }
    };
    ({$($buttons:tt)*} {$($messages:tt)*}) => {
        impl Command {
            /// Calls the correct `ButtonAction` when an inline keyboard button is pressed.
            #[allow(dead_code)]
            pub fn on_button(
                self,
                callback: ::morse::Callback,
                data: $crate::shared::SharedData,
            ) {
                match (self, callback, data) {
                    $($buttons)*
                    _ => unimplemented!()
                };
            }

            /// Calls the correct `MessageAction` when a non-command message is sent.
            #[allow(dead_code)]
            pub fn on_message(
                self,
                message: ::morse::Message,
                data: $crate::shared::SharedData,
            ) {
                match (self, message, data) {
                    $($messages)*
                    _ => unimplemented!()
                };
            }
        }
    }
}

macro_rules! commands {
    ($($t:ident $(: $mark:ident $(+ $marks:tt)*)?, $(#[doc = $doc:tt])?)*) => {
        use $crate::telebot::traits::*;

        /// All the commands that can be sent to the bot.
        #[derive(Clone, Copy, Debug, PartialEq)]
        #[repr(u8)]
        pub enum Command {
            $(
                $(#[doc = $doc])?
                $t,
            )*
        }

        $(
        pub use command::$t;
        )*
        mod command {
            use super::*;

            pub(super) trait CommandBase: Sized {
                fn to_command(&self) -> Command;
                fn new(msg: ::morse::ChatMessage, data: $crate::shared::SharedData) -> Self;
                fn msg(&self) -> &::morse::ChatMessage;
                fn shared_data(&self) -> &$crate::shared::SharedData;
            }

            $(#[allow(dead_code)]
            #[derive(Clone)]
            $(#[doc = $doc])?
            pub struct $t {
                __do_not_use_msg: ::morse::ChatMessage,
                pub data: $crate::shared::SharedData,
            })*

            $(
            impl CommandBase for $t {
                fn to_command(&self) -> Command {
                    Command::$t
                }

                fn new(msg: ::morse::ChatMessage, data: $crate::shared::SharedData) -> Self {
                    Self {
                        __do_not_use_msg: msg,
                        data,
                    }
                }

                fn msg(&self) -> &::morse::ChatMessage {
                    &self.__do_not_use_msg
                }

                fn shared_data(&self) -> &$crate::shared::SharedData {
                    &self.data
                }
            }
            )*

            impl Command {
                /// Calls the correct `CommandAction` when a command is sent.
                pub fn on_command(
                    self,
                    cmd: ::morse::Command,
                    data: $crate::shared::SharedData,
                ) {
                    match self {
                        $(Self::$t => $t::new(cmd.msg, data).on_command(cmd.text),)*
                    }
                }
            }

            impl TryFrom<u8> for Command {
                type Error = ();
                fn try_from(num: u8) -> Result<Self, ()> {
                    generate_try_from_u8!{{0} {$($t,)*} {num}}
                }
            }

            impl TryFrom<&str> for Command {
                type Error = ();
                fn try_from(name: &str) -> Result<Self, ()> {
                    $(
                    if name == $crate::telebot::to_snake_case(stringify!($t)) {
                        Ok(Command::$t)
                    } else
                    )* {
                        Err(())
                    }
                }
            }

            mark_muncher!{ {} {} $($t {$($mark $($marks)*)?},)* }
        }
    };
}

macro_rules! generate_try_from_u8 {
    ({$num:expr} {$item:tt, $($items:tt,)*} {$number:ident}) => {
        if $number == $num {
            Ok(Command::$item)
        } else { generate_try_from_u8!({$num + 1} {$($items,)*} {$number}) }
    };
    ({$num:expr} {} {$number:ident}) => {
        {
            Err(())
        }
    }
}
