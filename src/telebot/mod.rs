#[macro_use]
mod macros;
mod actions;
mod bot;
mod commands;
pub mod handlers;
mod traits;

pub use actions::Command;
use actions::*;
pub use bot::{PANIC_FLAG, TeleBot};

use morse::ChatId;
use std::{cell::RefCell, collections::HashMap};

thread_local! {
    static LISTENING: RefCell<HashMap<ChatId, Command>> = RefCell::new(HashMap::new());
}

fn to_snake_case(string: &str) -> String {
    let mut new_string = String::with_capacity(string.len());
    for ascii in string.chars() {
        if ascii.is_ascii_uppercase() {
            if !new_string.is_empty() {
                new_string.push('_')
            }
            new_string.push(ascii.to_ascii_lowercase());
        } else {
            new_string.push(ascii);
        }
    }
    new_string
}
