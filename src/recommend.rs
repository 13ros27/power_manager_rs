use crate::config::*;

/// Recommends a charge rate based on the estimated current usage of the house
/// and various config settings.
pub fn recommend(estimated: f32, config: &Config, soc: Option<u8>) -> i8 {
    let price = config.get(EnergyPrice);
    let mut charge_cost_limit = config.get(ChargeCostLimit);

    // If the soc is greater than the max paid soc set the ccl to 0 (stop charging).
    if soc.is_some_and(|soc| soc >= config.get(MaxPaidSoc)) {
        charge_cost_limit = 0.0;
    }

    if price < charge_cost_limit {
        32
    } else if estimated <= 0.0 {
        round_estimation(estimated, 1.0 - (charge_cost_limit / price).min(1.0), 3)
    } else {
        let mut discharge_value = if estimated < 3.0 {
            config.get(LowDischargeValue)
        } else {
            config.get(DischargeValue)
        };

        // If the soc is less than the min discharge soc set the discharge value to infinity (stop discharging).
        if soc.is_some_and(|soc| soc <= config.get(MinDischargeSoc)) {
            discharge_value = f32::INFINITY;
        }

        if price < discharge_value {
            0
        } else {
            round_estimation(
                estimated,
                (discharge_value / price).min(1.0),
                config.get(MinDischargeRate),
            )
        }
    }
}

fn round_estimation(estimated: f32, frac: f32, minimum: u8) -> i8 {
    let positive = estimated.abs();
    let value = if positive > minimum as f32 {
        if positive.rem_euclid(1.0) < frac {
            positive.floor() as i8
        } else {
            positive.ceil() as i8
        }
    } else if positive >= frac * minimum as f32 {
        minimum as i8
    } else {
        0
    };

    if estimated >= 0.0 { -value } else { value }
}
